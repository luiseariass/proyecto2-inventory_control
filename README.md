# Control de Inventario
## Author: Luis Enrique Arias Serrano
**Control de Inventario** es una aplicación para gestionar el inventario de una 
empresa de reciclaje, en la aplicacion puedes hacer ordenes de compra, ordenes
de venta, asi como darle seguimiento a los almacenes, a los empleados.
Los usuarios de la aplicación es el personal interno de la empresa. 
La aplicación ofrece un Dashboard para ver como ha flutuado las compras y las ventas durante el tiempo.
La aplicación cuenta con distinto tipos de usuarios y cada usuario tiene accesos distintos la página.

 
Usuarios
============
1. Regular: Puede crear y actualizar a los clientes asi como a las ordenes de compra y venta.
    * usuario: open_wkbmhar_user@tfbnw.net (Usuario Facebook)
    * contraseña: CLAVEprueba1
2. Admin: Los mismos permisos que el usuario regular, pero tiene acceso a los dashboard de información asi como a los inventarios.
    * usuario: pruebaproyecto2kavak@gmail.com (Usuario Google)
    * contraseña: CLAVEprueba1
3. SuperAdmin: Los de del usuario Admin, y crear nuevos almacenes asi como la gestión de los empleados
    * usuario: leticiacarrenoprueba1@mailinator.com 
    * contraseña: CLAVEprueba1  
Puedes visitar la aplicación yendo a este <a href="https://inventorycontrolns.herokuapp.com/">link</a>



Sobre la relación de las entidades
============
* Usuario: Esta relaciona con un almacen y con varias ordenes
* Cliente: Esta relacionado con varias orden
* Almacen: Esta relacionado con varios usuarios y con varias ordenes
* Orden: Esta relacionado con un usuario, un almacen y un cliente, la orden tendra los productos de la orden, asi como el tipo de esta orden debido a que puede ser de compra o venta y la fecha de la orden.

Para mas información les dejo la imagen de mis ERD: 
<img src = "ERD.png" width="600">

To do list
============

* Que el usuario regular solo pueda ver las ordenes que el creo
* Que Admin solo pueda ver los datos del almacen al cual pertenece



Creditos
============

* KAVAK Intern Program 