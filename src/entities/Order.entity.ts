import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinColumn, CreateDateColumn } from 'typeorm';

import { Client } from './Client.entity';
import { Store } from './Store.entity';
import { User } from './User.entity';
export enum Product {
  AL_HARD = "Hard Aluminum",
  AL_CANS =  "Aluminum Cans",
  AL_MIXT =  "Mixt Aluminum",
  CU = "Copper",
  BR = "Bronze",
  ST = "Steel"
}
export enum PaymentStatus {
  PENDING = "Pending to pay",
  PAID =  "Paid Out",
}
export enum TypeOrder {
  PURCHASE = "Purcharse Order",
  SALES =  "Sales Order",
}
export type Products = {
  name : Product,
  price : number,
  quantity : number
}
@Entity()
export class Order {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @ManyToOne(type => User, user => user.email)
  @JoinColumn({ name: 'user_email' })
  orderGenerator: User;
  @ManyToOne(type => Client, client => client.order)
  @JoinColumn({ name: 'client_name' })
  client: Client;
  @ManyToOne(type => Store, store => store.user)
  @JoinColumn({ name: 'store_name' })
  store: Store;
  @Column("simple-array")
  product: Product[]
  @Column("simple-array")
  price: number[]
  @Column("simple-array")
  quantity: number[]
  @Column({nullable: true})
  create_at : Date; 
  @Column({
    type : "enum",
    enum : PaymentStatus,
    default : PaymentStatus.PENDING
  })
  status: PaymentStatus;
  @Column({nullable:true})
  proofOfPayment: string
  @Column({
    type : "enum",
    enum : TypeOrder,
  })
  typeOrder: TypeOrder;
  @Column({nullable:true,type:"numeric"})
  amount:number
  @CreateDateColumn({nullable: true})
  create_colum_at : Date; 
  
}