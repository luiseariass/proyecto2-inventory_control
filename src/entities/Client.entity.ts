import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinColumn, OneToMany } from 'typeorm';

import { Order } from './Order.entity';
export enum Banks {
  AFIRME = "Banco Afime",
  AZTECA = "Banco Azteca",
  BANORTE = "Banorte",
  BBVA = "BBVA",
  HSBC = "HSBC",
  SANTANDER = "Banco Santander",
  INBURSA = "Banco Inbursa",
  SCOTIABANK = "Scotiabank",
  CITIBANAMEX = "Citibanamex"
}
@Entity()
export class Client {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @Column()
  email : string;
  @Column()
  name : string;
  @Column({nullable:true})
  clabe : string;
  @Column()
  bank : Banks; 
  @OneToMany(type => Order, order => order.client)
  @JoinColumn({name: 'order_id'})
  order: Order[];
}