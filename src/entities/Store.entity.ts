import { Entity, Column, PrimaryGeneratedColumn, JoinColumn, OneToMany } from 'typeorm';

import { Order } from './Order.entity';
import { User } from './User.entity';
@Entity()
export class Store {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @Column()
  name: string;
  @OneToMany(type => Order, order => order.store)
  @JoinColumn({name: 'order_id'})
  order: Order[];
  @OneToMany(type => User, user => user.store)
  @JoinColumn({name: 'user_id'})
  user: User[];
  @Column("simple-json")
  location: { address: string, coordinates: number[] } ;
}