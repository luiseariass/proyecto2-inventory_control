import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, ManyToMany, JoinColumn, OneToMany } from 'typeorm';

import { Order } from './Order.entity';
import { Store } from './Store.entity';
export enum Role {
  BASIC = "Basic",
  REGULAR = "Regular",
  ADMIN = "Admin",
  SUPERADMIN = "SuperAdmin"
}
@Entity()
export class User {
  @PrimaryGeneratedColumn("uuid")
  id: string;
  @Column({nullable:true})
  name : string;
  @Column()
  email: string;
  @Column({nullable:true})
  password: string;
  @Column({nullable:true})
  googleId: string;
  @Column({nullable:true})
  facebookId: string;
  @Column({
    type : "enum",
    enum : Role,
    default : Role.BASIC
  })
  role: Role;
  @OneToMany(type => Order, order => order.orderGenerator)
  @JoinColumn({name: 'order_id'})
  order: Order[]; 
  @Column({default:false})
  status : boolean;
  @ManyToOne(type => Store, store => store.user)
  @JoinColumn({name: 'store_id'})
  store: Store; 
  

}