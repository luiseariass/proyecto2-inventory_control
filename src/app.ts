#!/usr/bin/env node
require('dotenv').config();

import 'reflect-metadata'
import express from 'express';

import favicon from 'serve-favicon';

import hbs from 'hbs';

import logger from 'morgan';

import path from 'path';

import { createConnection } from 'typeorm';

import ExpressSession from 'express-session';

import flash from 'connect-flash';

import { TypeormStore } from 'connect-typeorm/out';

import cors from 'cors';

import passport from './config/passport.config';
import { Session } from './entities/Session.entity';
import { authGlobal,accessAPI, isLoggedIn, isActive} from './middlewares/auth.middleware';
import apiClientRoutes from './routes/api.client.routes';
import apiOrderRoutes from './routes/api.order.routes';
import apiStoreRoutes from './routes/api.store.routes';
import apiUserRoutes from './routes/api.user.routes';
import authRoutes from './routes/auth.routes';
import indexRoutes from './routes/index.routes';
import sideBarRoutes from './routes/sidebar.routes'

async function bootstrap() {
  const app_name = require('../package.json').name;
  const debug = require('debug')(`${app_name}:${path.basename(__filename).split('.')[0]}`);

  const database_url = process.env.Env === 'development' ? `postgres://${process.env.DB_USER}:${process.env.DB_PASSWORD}@localhost:5432/${process.env.DB}`: process.env.DATABASE_URL
  console.log(database_url)

  //DB connection 
  const connection = await createConnection({
    type: "postgres",
    url: database_url,
    entities: [path.join(__dirname, './entities/**/*.{ts,js}') ],
    migrations: [path.join(__dirname, './migrations/**/*.{ts,js}') ],
    cli: {
        "migrationsDir": "migrations"
    },
    synchronize: true
  })
  // display DB info
  console.log("connected to PostgreSQL! Database options: ", connection.options );

  // setup an express app
  const app = express();

  // Middleware Setup
  app.use(cors());
  app.use(logger('dev'));
  app.use(express.json());
  app.use(express.urlencoded({ extended: false }));

  // Express View engine setup
  app.use(require('node-sass-middleware')({
    src:  path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    sourceMap: true
  }));
        
  app.set('views', path.join(__dirname, 'views'));
  app.set('view engine', 'hbs');
  hbs.registerPartials( path.join(__dirname, 'views/partials' ) )
  app.use(express.static(path.join(__dirname, 'public')));
  app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));


  // Configure express-session
  app.use(ExpressSession({
    secret: process.env.SECRET,
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 1000 * 60 * 60 * 24 },
    store: new TypeormStore({
      cleanupLimit: 2,
      limitSubquery: false,
      ttl: 86400 
    }).connect(connection.getRepository(Session)) 
  }));

  // passport
  app.use(flash());
  app.use(passport.initialize());
  app.use(passport.session());

  // default value for title local
  app.locals.title = "Control de Inventario";
  app.use('/',authGlobal, indexRoutes);
  app.use('/',authGlobal,authRoutes);
  app.use('/',isLoggedIn,isActive,authGlobal,sideBarRoutes);
  app.use('/api/order',accessAPI,apiOrderRoutes);
  app.use('/api/user',accessAPI,apiUserRoutes);
  app.use('/api/store',accessAPI,apiStoreRoutes);
  app.use('/api/client',accessAPI,apiClientRoutes);

  return app;
}

module.exports = bootstrap;
