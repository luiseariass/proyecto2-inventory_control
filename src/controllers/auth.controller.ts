import { Request, Response, NextFunction } from 'express';

import { getRepository } from 'typeorm';

import bcryptjs from 'bcryptjs';

import passport from '../config/passport.config';
import { User } from '../entities/User.entity';
//SIGNUP 
export const getSignup = (req: Request, res: Response, next: NextFunction) => {
  res.render('auth/signup')
}
export const postSignup = (req: Request, res: Response, next: NextFunction) => {
  req.session.destroy(() => {
    req.logOut();
    res.clearCookie('graphNodeCookie');
  });
  const { name, email, password } = req.body;

  if(email === '' || password === ''){
    res.render("auth/signup", { message: "El Correo y la contraseña son requeridos"})
  }
  
  //Verificando si la contraseña cumple con los requerimientos, tiene que tener un tamaño minimo de 8 y
  //contener al menos una minuscula, una mayuscula y un numero
  const verifiedOneLowerCaseLetter = /[a-z]/.test(password);
  const verifiedOneUpperCaseLetter = /[A-Z]/.test(password);
  const verifiedOneNumber = /[0-9]/.test(password);
  if (password.length < 8 ||  !verifiedOneLowerCaseLetter || !verifiedOneUpperCaseLetter || !verifiedOneNumber){
    res.render("auth/signup", { message: "La contraseña no cumple con las condiciones requeridas", message2 : "Tiene que tener un tamaño de minimo de 8 caracteres", message3: "Debe poseer una mayuscula, una miniscula y un numero"})
  }
  //Verificando que el email tenga la estructura correcta
  const verifedEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(email);
  if (!verifedEmail){
    res.render("auth/signup", { message: "Verifique el correo introducido"})
  }
  getRepository(User).findOne({ email }).then(user => {
    console.log("entrando")
    if(!user) {
      const salt = bcryptjs.genSaltSync(10);
      const hashPassword = bcryptjs.hashSync(password, salt); 
      const newUser = getRepository(User).create({ name, email, password: hashPassword });
      getRepository(User)
      .save(newUser)
      .then(() => res.redirect('/confirmation'))
      .catch(err => console.log(err))
    } else {
      res.render('auth/signup', { message: "Este Correo esta en uso" });
    }
  }).catch(err => console.log(err));
}

//LOGIN
export const getLogin = (req: any, res: Response, next: NextFunction) => {
  let message = req.flash("error")[0];
  res.render('auth/login' , { message })
}
export const postLogin = passport.authenticate("local", {
  successRedirect: "/",
  failureRedirect: "/login",
  failureFlash: true
})

//LOGOUT
export const logout = (req: any, res: Response, next: NextFunction) => {
  req.session.destroy(() => {
    req.logOut();
    res.clearCookie('graphNodeCookie');
    res.status(200);
    res.redirect('/login');
   });
}
export const getConfirmation = (req: any, res: Response, next: NextFunction) => {
  res.render('auth/confirmation',{firstMail:true})
}
export const getGoogle = (passport.authenticate("google", {
  scope: [
    "https://www.googleapis.com/auth/userinfo.profile",
    "https://www.googleapis.com/auth/userinfo.email"
  ]
})
);
export const getGoogleCB=(passport.authenticate("google", {
  successRedirect: "/",
  failureRedirect: "/login"
})
);

export const getFacebook = (passport.authenticate("facebook", {
scope : ['email'] 
})
);


export const getFacebookCB=(passport.authenticate("facebook", {
successRedirect: "/",
failureRedirect: "/login"
})
);  


