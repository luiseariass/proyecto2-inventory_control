import { Request, Response, NextFunction } from 'express';

import { getRepository, getConnection } from 'typeorm';

import { Store } from '../entities/Store.entity';

export const createStore = (req: Request, res: Response, next: NextFunction ): void => {
    const { name,address, longitude,latitude} = req.body;
    const newStore = getRepository(Store).create({ name,location : {address,coordinates: [longitude,latitude]}}); 
    getRepository(Store).save(newStore)
    .then(savedStore => res.status(201).json( { message: "Store Created" }))
    .catch(err => res.status(404).json({err}))
  }

export const getAllStore = (req: Request, res: Response, next: NextFunction) => {
    getRepository(Store).find({relations:["user","order"]})
    .then(Store =>  res.status(200).json( {Store}) )
    .catch(err => res.status(404).json({err}))
}
export const getOneStore = (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    console.log("GET one stores");
    getRepository(Store).findOne({where : {id : id}, relations : ["user","order"]} )
    .then(store =>  res.status(200).json(store) )
    .catch(err => res.status(404).json({err}))
}

export const updateStore = async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    const { name,address, longitude,latitude} = req.body;
    console.log("dentro del API",id)
    await getConnection()
    .createQueryBuilder()
    .update(Store)
    .set({ name,location : {address:address,coordinates: [longitude,latitude]}})
    .where("id = :id", { id: id })
    .execute()
    .then(store =>  res.status(200).json(store) )
    .catch(err => res.status(404).json({err}))
}

export const deleteStore = (req: Request, res: Response, next: NextFunction ): void => {
    const { id } = req.params;
    getRepository(Store).delete(id)
    .then(() => res.status(200).json({ message: "Target earesed" }))
    .catch(err => console.log(err));
  }