import { Request, Response, NextFunction } from 'express';

import { getRepository, getConnection, Between, createQueryBuilder } from 'typeorm';

import { Client } from '../entities/Client.entity';
import { Order,TypeOrder} from '../entities/Order.entity';
import { request } from 'http';

export const createClient = (req: Request, res: Response, next: NextFunction ): void => {
    const { email,name,clabe, bank} = req.body;
    const newClient = getRepository(Client).create({email,name,clabe,bank}); 
    getRepository(Client).save(newClient)
    .then(savedClient => res.status(201).json( { message: "Client Created" }))
    .catch(err => res.status(404).json({err}))
  }

export const getAllClient = (req: Request, res: Response, next: NextFunction) => {
    getRepository(Client).find({relations:["order"]})
    .then(client =>  res.status(200).json( {client}) )
    .catch(err => res.status(404).json({err}))
}
export const getOneClient = (req: Request, res: Response, next: NextFunction) => {
    const { clientID } = req.params
    getRepository(Client).findOne({where : clientID, relations : ["order"]} )
    .then(client =>  res.status(200).json(client) )
    .catch(err => res.status(404).json({err}))
}

export const updateClient = async (req: Request, res: Response, next: NextFunction) => {
    const {id} = req.params
    const {name,email,bank,clabe} = req.body;
    console.log({id, name,email,bank,clabe})
    await getConnection()
    .createQueryBuilder()
    .update(Client)
    .set({email,name,clabe,bank})
    .where("id = :id", { id: id })
    .execute()
    .then(client =>  res.status(200).json(client))
    .catch(err => {
        console.log(err)
        res.status(404).json({err})})
}

export const deleteClient = (req: Request, res: Response, next: NextFunction ): void => {
    const { clientId } = req.params;
    getRepository(Client).delete(clientId)
    .then(() => res.status(200).json({ message: "Target earesed" }))
    .catch(err => console.log(err));
}

export const getProviderByDate = async (req: Request, res: Response, next: NextFunction) => {
    const {yearStart, monthStart, dayStart,yearEnd, monthEnd, dayEnd } = req.params
    await getRepository(Client)
    .createQueryBuilder("client")
    .innerJoinAndSelect("client.order", "order")
    .where("order.typeOrder = :typeOrder",{ typeOrder: TypeOrder.PURCHASE})
    .andWhere(`order.create_at Between :start AND :end`,{ start : new Date(Number(yearStart),Number(monthStart)-1,Number(dayStart)),end : new Date(Number(yearEnd),Number(monthEnd)-1,Number(dayEnd))})
    .getOne().then(order =>  res.status(200).json(order))
    .catch(err => res.status(404).json({err}))
    
}
export const getClientByDate = async (req: Request, res: Response, next: NextFunction) => {
    const {yearStart, monthStart, dayStart,yearEnd, monthEnd, dayEnd } = req.params
    await getRepository(Client)
    .createQueryBuilder("client")
    .innerJoinAndSelect("client.order", "order")
    .where("order.typeOrder = :typeOrder",{ typeOrder: TypeOrder.SALES})
    .andWhere(`order.create_at Between :start AND :end`,{ start : new Date(Number(yearStart),Number(monthStart)-1,Number(dayStart)),end : new Date(Number(yearEnd),Number(monthEnd)-1,Number(dayEnd))})
    .getOne().then(order =>  res.status(200).json(order))
    .catch(err => res.status(404).json({err}))
}

