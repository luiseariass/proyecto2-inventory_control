import { Request, Response, NextFunction } from 'express';

import { getRepository, getConnection, Between, Not } from 'typeorm';

import { Order,TypeOrder, PaymentStatus} from '../entities/Order.entity';

export const createOrder = (req: Request, res: Response, next: NextFunction ): void => {
    const {product ,client ,store,price,typeOrder,quantity,create_at} = req.body
    console.log(create_at)
    let amount = 0;
    if (price.length){
        for (let i=0; i<price.length;i++){
            amount+=Number(price[i])*Number(quantity[i]);   
    }}else{
        amount = Number(price) * Number(quantity)
    }
    amount = Number(amount)
    const newOrder = getRepository(Order).create({ store,create_at,product,orderGenerator:req.session.passport.user,typeOrder,price,quantity,amount : amount,client});
  
    getRepository(Order).save(newOrder)
    .then(savedOrder => res.status(201).json( { message: "Order Created" }))
    .catch(err => console.log(err));
  }

export const getAllOrder = (req: Request, res: Response, next: NextFunction) => {
    getRepository(Order).find({relations:["orderGenerator","store","client"]})
    .then(order =>  res.status(200).json( { order }) )
    .catch(err => console.log(err));
}
export const getOneOrder = (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    getRepository(Order).findOne({relations:["orderGenerator","store","client"],where: {id:id}})
    .then(order =>  res.status(200).json(order) )
    .catch(err => res.status(404).json({err}))
}

export const updateOrderStatus = async (req: Request, res: Response, next: NextFunction) => {
    const { id,status,typeOfOrder} = req.body;
    console.log(status)
    await getConnection()
    .createQueryBuilder()
    .update(Order)
    .set({ status : status})
    .where("id = :id", { id: id })
    .execute()
    .then(store =>  res.status(200).json(store) )
    .catch(err => res.status(404).json({err}))
}

export const deleteOrder = (req: Request, res: Response, next: NextFunction ): void => {
    const { orderId } = req.params;
    getRepository(Order).delete(orderId)
    .then(() => res.status(200).json({ message: "Target earesed" }))
    .catch(err => console.log(err));
}


export const getPurchaseOrderByDate = async (req: Request, res: Response, next: NextFunction) => {
    const {yearStart, monthStart, dayStart,yearEnd, monthEnd, dayEnd } = req.params
    const dateBetween = (dateStart: Date,dateEnd:Date) => Between(dateStart, dateEnd);
    const valueBetween = dateBetween((new Date(Number(yearStart),Number(monthStart)-1,Number(dayStart))),new Date(Number(yearEnd),Number(monthEnd)-1,Number(dayEnd)))
    getRepository(Order).find({
        relations:["orderGenerator","store","client"],
        where:[
        {typeOrder:TypeOrder.PURCHASE, create_at:valueBetween}
        ],
        order:{status:1,create_at:1 , create_colum_at:1}
    })
    .then(order =>  res.status(200).json(order))
    .catch(err => res.status(404).json({err}))
}
export const getSalesOrderByDate = async (req: Request, res: Response, next: NextFunction) => {
    const {yearStart, monthStart, dayStart,yearEnd, monthEnd, dayEnd } = req.params
    const dateBetween = (dateStart: Date,dateEnd:Date) => Between(dateStart, dateEnd);
    const valueBetween = dateBetween((new Date(Number(yearStart),Number(monthStart)-1,Number(dayStart))),new Date(Number(yearEnd),Number(monthEnd)-1,Number(dayEnd)))
    getRepository(Order).find({
        relations:["orderGenerator","store","client"],
        where:[
        {typeOrder:TypeOrder.SALES, create_at:valueBetween}
        ],
        order:{status:1,create_at:1, create_colum_at:1}
    })
    .then(order => res.status(200).json(order))
    .catch(err => res.status(404).json({err}))
}