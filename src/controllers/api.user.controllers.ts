import { Request, Response, NextFunction } from 'express';

import { getRepository, getConnection } from 'typeorm';

import { User } from '../entities/User.entity';
import { sendEmail } from '../config/nodemailer.config';


export const getAllUser = (req: Request, res: Response, next: NextFunction) => {
    getRepository(User).find({relations: ["order","store"],order:{name:1}} )
    .then(user => res.status(200).json( { user }) )
    .catch(err => res.status(404).json({err}));
}
export const getOneUser = (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    getRepository(User).findOne({relations: ["order","store"],where : {id:id}})
    .then(user =>  res.status(200).json(user) )
    .catch(err => res.status(404).json({err}))
}

export const updateUserStatus = async (req: Request, res: Response, next: NextFunction) => {
    let {id} =req.params
    let {role, status,store,email,name} = req.body;
    //console.log(req.body)
    if (role==="SuperAdmin"){
        store = null 
    }
    if (status === false){
        getRepository(User).delete({id : id})
        .then(async() => {
            if (req.session.passport.user === id){
                req.session.destroy(() => {
                    req.logOut();
                    res.clearCookie('graphNodeCookie');
                    res.status(200);
                    res.redirect('/login');
                });
            }
            await sendEmail(email,`$${process.env.ENDPOINT}`,status,name)
            res.status(200).json({ message: "Target earesed" })
        })
        .catch(err => console.log(err));

    }else{
        let setValue = {}
        if (store === ''){
            setValue = {role:role,status:status}
        }else{
            setValue = {role:role,status:status,store:store}
        }
        console.log(id)
        await getConnection()
        .createQueryBuilder()
        .update(User)
        .set(setValue)
        .where("id = :id", { id: id })
        .execute()
        .then(async user =>  {
        await sendEmail(email,`$${process.env.ENDPOINT}`,status,name)
        res.status(200).json(user)} )
    .catch(err => {
        console.log(err)
        res.status(404).json({err})
    })
    }
    


    
}

export const deleteUser = (req: Request, res: Response, next: NextFunction ): void => {
    const { id } = req.params;
    getRepository(User).delete(id)
    .then(() => res.status(200).json({ message: "Target earesed" }))
    .catch(err => console.log(err));
}

export const getAllUserActive = (req: Request, res: Response, next: NextFunction) => {
    if (req.params.status === "true"){
        getRepository(User).find({relations: ["order","store"],
        where:[
            {status:req.params.status, role:"SuperAdmin"},
            {status:req.params.status, role:"Regular"},
            {status:req.params.status, role:"Admin"}
        ]
        })
        .then(user =>  res.status(200).json( { user }) )
        .catch(err => res.status(404).json({err}));
    }else{
        getRepository(User).find({relations: ["order","store"],where:{status:req.params.status}})
        .then(user =>  res.status(200).json( { user }) )
        .catch(err => res.status(404).json({err}));
    }
    
}