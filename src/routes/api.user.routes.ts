import { Router } from 'express';

import { getAllUser, getOneUser, updateUserStatus, deleteUser, getAllUserActive } from '../controllers/api.user.controllers';
 
const router = Router();
router.get('/all',getAllUser)
router.get('/:id',getOneUser);
router.put('/:id',updateUserStatus);
router.delete('/id', deleteUser);
router.get('/all/active/:status', getAllUserActive);

export default router;