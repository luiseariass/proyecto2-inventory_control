import { Router } from 'express';

import { deleteClient, getAllClient, getOneClient, updateClient, createClient, getProviderByDate, getClientByDate } from '../controllers/api.client.controllers';
 
const router = Router();
router.post('/',createClient)
router.get('/all',getAllClient)
router.get('/:id',getOneClient);
router.put('/:id',updateClient);
router.delete('/id', deleteClient);
router.get('/provider/date/:yearStart/:monthStart/:dayStart&:yearEnd/:monthEnd/:dayEnd',getProviderByDate)
router.get('/client/date/:yearStart/:monthStart/:dayStart&:yearEnd/:monthEnd/:dayEnd',getClientByDate)
export default router;