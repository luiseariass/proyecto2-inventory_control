import { Router } from 'express';

import { deleteStore, getAllStore, getOneStore, updateStore, createStore } from '../controllers/api.store.controllers';
 
const router = Router();
router.post('/',createStore)
router.get('/all',getAllStore)
router.get('/:id',getOneStore);
router.put('/:id',updateStore); //Si no eres administrador solo puedes modificar el status de la orden
router.delete('/id', deleteStore);

export default router;