import { Router, Request, Response, NextFunction } from 'express';
import { permissionSuperAdmin, permissionAdmin } from '../middlewares/auth.middleware';

const router = Router();

router.get('/index', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/index',{user:req.user});
});

router.get('/dashboard',permissionAdmin, (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/dashboard');
});

router.get('/compras', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/buy',{comesFromAnOrder : false});
});
router.get('/compras/1', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/buy',{comesFromAnOrder : "buy"});
});

router.get('/ventas', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/sell');
});
router.get('/ventas/1', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/sell',{comesFromAnOrder : "sell"});
});

router.get('/inventario',permissionAdmin, (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/inventory');
});


router.get('/empleados',permissionSuperAdmin, (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/employees');
});
router.get('/clientes', (req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/providerclient');
});
router.get('/tiendas', permissionSuperAdmin,(req: Request, res: Response, next: NextFunction ): void => {
  res.render('sidebar/store');
});
export default router;