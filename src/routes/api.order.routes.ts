import { Router } from 'express';

import { getAllOrder, getOneOrder, updateOrderStatus, deleteOrder, createOrder, getPurchaseOrderByDate, getSalesOrderByDate } from '../controllers/api.order.controller';
import uploadCloud from '../config/cloudinary.config';
 
const router = Router();
router.post('/',createOrder)
router.get('/all',getAllOrder)
router.get('/:id',getOneOrder);
router.delete('/:id', deleteOrder);
router.get('/purchaseorder/date/:yearStart/:monthStart/:dayStart&:yearEnd/:monthEnd/:dayEnd',getPurchaseOrderByDate)
router.get('/salesorder/date/:yearStart/:monthStart/:dayStart&:yearEnd/:monthEnd/:dayEnd',getSalesOrderByDate)
router.put('/update',updateOrderStatus);
export default router;