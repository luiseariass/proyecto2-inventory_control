import { Router } from 'express';

import { 
  getSignup, 
  postSignup, 
  getLogin, 
  postLogin, 
  logout,
  getGoogle,
  getGoogleCB,
  getFacebook,
  getFacebookCB,
  getConfirmation, 
} from '../controllers/auth.controller';

const router = Router();

router.post('/signup', postSignup);
router.get('/signup', getSignup );
router.get('/confirmation',getConfirmation);
router.post('/login', postLogin);
router.get('/login', getLogin);
router.get('/logout', logout);
router.get('/auth/google',getGoogle);
router.get('/auth/google/callback',getGoogleCB);
router.get('/auth/facebook',getFacebook);
router.get('/auth/facebook/callback',getFacebookCB);

export default router;