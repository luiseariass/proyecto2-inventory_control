const cloudinary = require("cloudinary").v2;
import { CloudinaryStorage } from 'multer-storage-cloudinary';
import multer from 'multer';

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_KEY,
  api_secret: process.env.CLOUDINARY_SECRET
});


const storage = new CloudinaryStorage({
  cloudinary,
  params: {
    folder: "inventoryControl", // foldername in cloudinary
    allowed_formats: ['pdf']
  },
});

const uploadCloud = multer({ storage: storage });

export default uploadCloud;
