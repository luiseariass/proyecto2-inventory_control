import passport from 'passport';
const LocalStrategy = require('passport-local').Strategy;
const googleStrategy = require('passport-google-oauth20').Strategy;
const facebookStrategy = require('passport-facebook').Strategy;
import bcryptjs from 'bcryptjs';
import { User } from '../entities/User.entity';
import { getRepository } from 'typeorm';

const verifyCallback = async (email, password, done) => {
  try {
    const user = await getRepository(User).findOne({ email: email });
    if(!user) {
      return done(null, false, { message: 'El correo no fue encontrado'})
    }
    if(!user.password) {
      return done(null, false, { message: 'Verifique que este registrado'})
    }
    const isValid = bcryptjs.compareSync(password, user.password);

    if(isValid) return done(null, user) 
    else        return done(null, false, { message: "Contraseña incorrecta"})

  } catch (err) {
    done("Hubo un error con el servidor", false, { message: 'Error interno del servidor'})
    console.log(err)
  }
}

const strategy = new LocalStrategy({
  usernameField: 'email',  
  passwordField: 'password'
}, verifyCallback);

passport.use(strategy);

passport.serializeUser((user: User, cb) => {
  cb(null, user.id);
});

passport.deserializeUser((id, cb) => {
  getRepository(User).findOne( id )
  .then(user => cb(null, user)) 
  .catch(error => cb(error, null));
});

// Configuracion de estrategia de Google
// Google 👇

const googleVerifyCallback = async (_, __, profile, done) => {
  const user = await getRepository(User).findOne({ googleId: profile.id }).catch(err =>
    done(err)
  );
  if (user) {
    await getRepository(User).save(user);
    done(null, user);
  } else {
    
    const newUser = getRepository(User).create({
      googleId: profile.id,
      name: profile.displayName,
      email: profile._json.email,
    });
    await getRepository(User).save(newUser);
    done(null, newUser);
  }
}


const gStrategy =  new googleStrategy({
    clientID: process.env.GOOGLE_ID,
    clientSecret: process.env.GOOGLE_SECRET,
    callbackURL: `${process.env.ENDPOINT}/auth/google/callback`
  }, googleVerifyCallback)

passport.use(gStrategy);



const facebookVerifyCallback = async (_, __, profile, done) => {
    const user = await getRepository(User).findOne({ facebookId: profile.id }).catch(err =>
      done(err)
    );
    if (user) {
      await getRepository(User).save(user);
      done(null, user);
    } else {
      
      const newUser = getRepository(User).create({
        facebookId: profile.id,
        name: profile.displayName,
        email: profile._json.email,
      });
      await getRepository(User).save(newUser);
      done(null, newUser);
    }
  }
  const fbStrategy =   new facebookStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET,
    callbackURL: `${process.env.ENDPOINT}/auth/facebook/callback`,
    profileFields: ['id', 'displayName', 'photos', 'email']
  }, facebookVerifyCallback)

passport.use(fbStrategy);  


export default passport;