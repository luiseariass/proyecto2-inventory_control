import nodemailer from "nodemailer"
import fs from 'fs';
import path from 'path';
import hbs from 'hbs';

console.log(__dirname);
const verificationTemplate = hbs.compile(
  fs.readFileSync( path.join(__dirname , '../views/template-email.hbs'), 'utf8' )
);


export async function sendEmail(to,url,status,name) {
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user : process.env.EMAIL,
          pass : process.env.PASS
        },
        tls: {
            rejectUnauthorized: false
        }
      });
    // send mail with defined transport object
    await transporter.sendMail({
      from: 'vindication@enron.com', // sender address
      to, // list of receivers
      subject: "Respuesta de tu solicitud", // Subject line
      html : verificationTemplate({ url ,status,name })
    }).then()
    .catch((error)=>console.log(error))
  }
  