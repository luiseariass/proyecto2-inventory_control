"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFacebookCB = exports.getFacebook = exports.getGoogleCB = exports.getGoogle = exports.getConfirmation = exports.logout = exports.postLogin = exports.getLogin = exports.postSignup = exports.getSignup = void 0;
var typeorm_1 = require("typeorm");
var bcryptjs_1 = __importDefault(require("bcryptjs"));
var passport_config_1 = __importDefault(require("../config/passport.config"));
var User_entity_1 = require("../entities/User.entity");
//SIGNUP 
exports.getSignup = function (req, res, next) {
    res.render('auth/signup');
};
exports.postSignup = function (req, res, next) {
    req.session.destroy(function () {
        req.logOut();
        res.clearCookie('graphNodeCookie');
    });
    var _a = req.body, name = _a.name, email = _a.email, password = _a.password;
    if (email === '' || password === '') {
        res.render("auth/signup", { message: "El Correo y la contraseña son requeridos" });
    }
    //Verificando si la contraseña cumple con los requerimientos, tiene que tener un tamaño minimo de 8 y
    //contener al menos una minuscula, una mayuscula y un numero
    var verifiedOneLowerCaseLetter = /[a-z]/.test(password);
    var verifiedOneUpperCaseLetter = /[A-Z]/.test(password);
    var verifiedOneNumber = /[0-9]/.test(password);
    if (password.length < 8 || !verifiedOneLowerCaseLetter || !verifiedOneUpperCaseLetter || !verifiedOneNumber) {
        res.render("auth/signup", { message: "La contraseña no cumple con las condiciones requeridas", message2: "Tiene que tener un tamaño de minimo de 8 caracteres", message3: "Debe poseer una mayuscula, una miniscula y un numero" });
    }
    //Verificando que el email tenga la estructura correcta
    var verifedEmail = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/.test(email);
    if (!verifedEmail) {
        res.render("auth/signup", { message: "Verifique el correo introducido" });
    }
    typeorm_1.getRepository(User_entity_1.User).findOne({ email: email }).then(function (user) {
        console.log("entrando");
        if (!user) {
            var salt = bcryptjs_1.default.genSaltSync(10);
            var hashPassword = bcryptjs_1.default.hashSync(password, salt);
            var newUser = typeorm_1.getRepository(User_entity_1.User).create({ name: name, email: email, password: hashPassword });
            typeorm_1.getRepository(User_entity_1.User)
                .save(newUser)
                .then(function () { return res.redirect('/confirmation'); })
                .catch(function (err) { return console.log(err); });
        }
        else {
            res.render('auth/signup', { message: "Este Correo esta en uso" });
        }
    }).catch(function (err) { return console.log(err); });
};
//LOGIN
exports.getLogin = function (req, res, next) {
    var message = req.flash("error")[0];
    res.render('auth/login', { message: message });
};
exports.postLogin = passport_config_1.default.authenticate("local", {
    successRedirect: "/",
    failureRedirect: "/login",
    failureFlash: true
});
//LOGOUT
exports.logout = function (req, res, next) {
    req.session.destroy(function () {
        req.logOut();
        res.clearCookie('graphNodeCookie');
        res.status(200);
        res.redirect('/login');
    });
};
exports.getConfirmation = function (req, res, next) {
    res.render('auth/confirmation', { firstMail: true });
};
exports.getGoogle = (passport_config_1.default.authenticate("google", {
    scope: [
        "https://www.googleapis.com/auth/userinfo.profile",
        "https://www.googleapis.com/auth/userinfo.email"
    ]
}));
exports.getGoogleCB = (passport_config_1.default.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/login"
}));
exports.getFacebook = (passport_config_1.default.authenticate("facebook", {
    scope: ['email']
}));
exports.getFacebookCB = (passport_config_1.default.authenticate("facebook", {
    successRedirect: "/",
    failureRedirect: "/login"
}));
