"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getClientByDate = exports.getProviderByDate = exports.deleteClient = exports.updateClient = exports.getOneClient = exports.getAllClient = exports.createClient = void 0;
var typeorm_1 = require("typeorm");
var Client_entity_1 = require("../entities/Client.entity");
var Order_entity_1 = require("../entities/Order.entity");
exports.createClient = function (req, res, next) {
    var _a = req.body, email = _a.email, name = _a.name, clabe = _a.clabe, bank = _a.bank;
    var newClient = typeorm_1.getRepository(Client_entity_1.Client).create({ email: email, name: name, clabe: clabe, bank: bank });
    typeorm_1.getRepository(Client_entity_1.Client).save(newClient)
        .then(function (savedClient) { return res.status(201).json({ message: "Client Created" }); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.getAllClient = function (req, res, next) {
    typeorm_1.getRepository(Client_entity_1.Client).find({ relations: ["order"] })
        .then(function (client) { return res.status(200).json({ client: client }); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.getOneClient = function (req, res, next) {
    var clientID = req.params.clientID;
    typeorm_1.getRepository(Client_entity_1.Client).findOne({ where: clientID, relations: ["order"] })
        .then(function (client) { return res.status(200).json(client); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.updateClient = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, name, email, bank, clabe;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, name = _a.name, email = _a.email, bank = _a.bank, clabe = _a.clabe;
                console.log({ id: id, name: name, email: email, bank: bank, clabe: clabe });
                return [4 /*yield*/, typeorm_1.getConnection()
                        .createQueryBuilder()
                        .update(Client_entity_1.Client)
                        .set({ email: email, name: name, clabe: clabe, bank: bank })
                        .where("id = :id", { id: id })
                        .execute()
                        .then(function (client) { return res.status(200).json(client); })
                        .catch(function (err) {
                        console.log(err);
                        res.status(404).json({ err: err });
                    })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
exports.deleteClient = function (req, res, next) {
    var clientId = req.params.clientId;
    typeorm_1.getRepository(Client_entity_1.Client).delete(clientId)
        .then(function () { return res.status(200).json({ message: "Target earesed" }); })
        .catch(function (err) { return console.log(err); });
};
exports.getProviderByDate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.params, yearStart = _a.yearStart, monthStart = _a.monthStart, dayStart = _a.dayStart, yearEnd = _a.yearEnd, monthEnd = _a.monthEnd, dayEnd = _a.dayEnd;
                return [4 /*yield*/, typeorm_1.getRepository(Client_entity_1.Client)
                        .createQueryBuilder("client")
                        .innerJoinAndSelect("client.order", "order")
                        .where("order.typeOrder = :typeOrder", { typeOrder: Order_entity_1.TypeOrder.PURCHASE })
                        .andWhere("order.create_at Between :start AND :end", { start: new Date(Number(yearStart), Number(monthStart) - 1, Number(dayStart)), end: new Date(Number(yearEnd), Number(monthEnd) - 1, Number(dayEnd)) })
                        .getOne().then(function (order) { return res.status(200).json(order); })
                        .catch(function (err) { return res.status(404).json({ err: err }); })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
exports.getClientByDate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.params, yearStart = _a.yearStart, monthStart = _a.monthStart, dayStart = _a.dayStart, yearEnd = _a.yearEnd, monthEnd = _a.monthEnd, dayEnd = _a.dayEnd;
                return [4 /*yield*/, typeorm_1.getRepository(Client_entity_1.Client)
                        .createQueryBuilder("client")
                        .innerJoinAndSelect("client.order", "order")
                        .where("order.typeOrder = :typeOrder", { typeOrder: Order_entity_1.TypeOrder.SALES })
                        .andWhere("order.create_at Between :start AND :end", { start: new Date(Number(yearStart), Number(monthStart) - 1, Number(dayStart)), end: new Date(Number(yearEnd), Number(monthEnd) - 1, Number(dayEnd)) })
                        .getOne().then(function (order) { return res.status(200).json(order); })
                        .catch(function (err) { return res.status(404).json({ err: err }); })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
