"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllUserActive = exports.deleteUser = exports.updateUserStatus = exports.getOneUser = exports.getAllUser = void 0;
var typeorm_1 = require("typeorm");
var User_entity_1 = require("../entities/User.entity");
var nodemailer_config_1 = require("../config/nodemailer.config");
exports.getAllUser = function (req, res, next) {
    typeorm_1.getRepository(User_entity_1.User).find({ relations: ["order", "store"], order: { name: 1 } })
        .then(function (user) { return res.status(200).json({ user: user }); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.getOneUser = function (req, res, next) {
    var id = req.params.id;
    typeorm_1.getRepository(User_entity_1.User).findOne({ relations: ["order", "store"], where: { id: id } })
        .then(function (user) { return res.status(200).json(user); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.updateUserStatus = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, role, status, store, email, name, setValue;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, role = _a.role, status = _a.status, store = _a.store, email = _a.email, name = _a.name;
                //console.log(req.body)
                if (role === "SuperAdmin") {
                    store = null;
                }
                if (!(status === false)) return [3 /*break*/, 1];
                typeorm_1.getRepository(User_entity_1.User).delete({ id: id })
                    .then(function () { return __awaiter(void 0, void 0, void 0, function () {
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                if (req.session.passport.user === id) {
                                    req.session.destroy(function () {
                                        req.logOut();
                                        res.clearCookie('graphNodeCookie');
                                        res.status(200);
                                        res.redirect('/login');
                                    });
                                }
                                return [4 /*yield*/, nodemailer_config_1.sendEmail(email, "$" + process.env.ENDPOINT, status, name)];
                            case 1:
                                _a.sent();
                                res.status(200).json({ message: "Target earesed" });
                                return [2 /*return*/];
                        }
                    });
                }); })
                    .catch(function (err) { return console.log(err); });
                return [3 /*break*/, 3];
            case 1:
                setValue = {};
                if (store === '') {
                    setValue = { role: role, status: status };
                }
                else {
                    setValue = { role: role, status: status, store: store };
                }
                console.log(id);
                return [4 /*yield*/, typeorm_1.getConnection()
                        .createQueryBuilder()
                        .update(User_entity_1.User)
                        .set(setValue)
                        .where("id = :id", { id: id })
                        .execute()
                        .then(function (user) { return __awaiter(void 0, void 0, void 0, function () {
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0: return [4 /*yield*/, nodemailer_config_1.sendEmail(email, "$" + process.env.ENDPOINT, status, name)];
                                case 1:
                                    _a.sent();
                                    res.status(200).json(user);
                                    return [2 /*return*/];
                            }
                        });
                    }); })
                        .catch(function (err) {
                        console.log(err);
                        res.status(404).json({ err: err });
                    })];
            case 2:
                _b.sent();
                _b.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };
exports.deleteUser = function (req, res, next) {
    var id = req.params.id;
    typeorm_1.getRepository(User_entity_1.User).delete(id)
        .then(function () { return res.status(200).json({ message: "Target earesed" }); })
        .catch(function (err) { return console.log(err); });
};
exports.getAllUserActive = function (req, res, next) {
    if (req.params.status === "true") {
        typeorm_1.getRepository(User_entity_1.User).find({ relations: ["order", "store"],
            where: [
                { status: req.params.status, role: "SuperAdmin" },
                { status: req.params.status, role: "Regular" },
                { status: req.params.status, role: "Admin" }
            ]
        })
            .then(function (user) { return res.status(200).json({ user: user }); })
            .catch(function (err) { return res.status(404).json({ err: err }); });
    }
    else {
        typeorm_1.getRepository(User_entity_1.User).find({ relations: ["order", "store"], where: { status: req.params.status } })
            .then(function (user) { return res.status(200).json({ user: user }); })
            .catch(function (err) { return res.status(404).json({ err: err }); });
    }
};
