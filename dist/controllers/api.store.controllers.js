"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteStore = exports.updateStore = exports.getOneStore = exports.getAllStore = exports.createStore = void 0;
var typeorm_1 = require("typeorm");
var Store_entity_1 = require("../entities/Store.entity");
exports.createStore = function (req, res, next) {
    var _a = req.body, name = _a.name, address = _a.address, longitude = _a.longitude, latitude = _a.latitude;
    var newStore = typeorm_1.getRepository(Store_entity_1.Store).create({ name: name, location: { address: address, coordinates: [longitude, latitude] } });
    typeorm_1.getRepository(Store_entity_1.Store).save(newStore)
        .then(function (savedStore) { return res.status(201).json({ message: "Store Created" }); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.getAllStore = function (req, res, next) {
    typeorm_1.getRepository(Store_entity_1.Store).find({ relations: ["user", "order"] })
        .then(function (Store) { return res.status(200).json({ Store: Store }); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.getOneStore = function (req, res, next) {
    var id = req.params.id;
    console.log("GET one stores");
    typeorm_1.getRepository(Store_entity_1.Store).findOne({ where: { id: id }, relations: ["user", "order"] })
        .then(function (store) { return res.status(200).json(store); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.updateStore = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var id, _a, name, address, longitude, latitude;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                id = req.params.id;
                _a = req.body, name = _a.name, address = _a.address, longitude = _a.longitude, latitude = _a.latitude;
                console.log("dentro del API", id);
                return [4 /*yield*/, typeorm_1.getConnection()
                        .createQueryBuilder()
                        .update(Store_entity_1.Store)
                        .set({ name: name, location: { address: address, coordinates: [longitude, latitude] } })
                        .where("id = :id", { id: id })
                        .execute()
                        .then(function (store) { return res.status(200).json(store); })
                        .catch(function (err) { return res.status(404).json({ err: err }); })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
exports.deleteStore = function (req, res, next) {
    var id = req.params.id;
    typeorm_1.getRepository(Store_entity_1.Store).delete(id)
        .then(function () { return res.status(200).json({ message: "Target earesed" }); })
        .catch(function (err) { return console.log(err); });
};
