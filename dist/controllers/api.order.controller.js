"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getSalesOrderByDate = exports.getPurchaseOrderByDate = exports.deleteOrder = exports.updateOrderStatus = exports.getOneOrder = exports.getAllOrder = exports.createOrder = void 0;
var typeorm_1 = require("typeorm");
var Order_entity_1 = require("../entities/Order.entity");
exports.createOrder = function (req, res, next) {
    var _a = req.body, product = _a.product, client = _a.client, store = _a.store, price = _a.price, typeOrder = _a.typeOrder, quantity = _a.quantity, create_at = _a.create_at;
    console.log(create_at);
    var amount = 0;
    if (price.length) {
        for (var i = 0; i < price.length; i++) {
            amount += Number(price[i]) * Number(quantity[i]);
        }
    }
    else {
        amount = Number(price) * Number(quantity);
    }
    amount = Number(amount);
    var newOrder = typeorm_1.getRepository(Order_entity_1.Order).create({ store: store, create_at: create_at, product: product, orderGenerator: req.session.passport.user, typeOrder: typeOrder, price: price, quantity: quantity, amount: amount, client: client });
    typeorm_1.getRepository(Order_entity_1.Order).save(newOrder)
        .then(function (savedOrder) { return res.status(201).json({ message: "Order Created" }); })
        .catch(function (err) { return console.log(err); });
};
exports.getAllOrder = function (req, res, next) {
    typeorm_1.getRepository(Order_entity_1.Order).find({ relations: ["orderGenerator", "store", "client"] })
        .then(function (order) { return res.status(200).json({ order: order }); })
        .catch(function (err) { return console.log(err); });
};
exports.getOneOrder = function (req, res, next) {
    var id = req.params.id;
    typeorm_1.getRepository(Order_entity_1.Order).findOne({ relations: ["orderGenerator", "store", "client"], where: { id: id } })
        .then(function (order) { return res.status(200).json(order); })
        .catch(function (err) { return res.status(404).json({ err: err }); });
};
exports.updateOrderStatus = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, id, status, typeOfOrder;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = req.body, id = _a.id, status = _a.status, typeOfOrder = _a.typeOfOrder;
                console.log(status);
                return [4 /*yield*/, typeorm_1.getConnection()
                        .createQueryBuilder()
                        .update(Order_entity_1.Order)
                        .set({ status: status })
                        .where("id = :id", { id: id })
                        .execute()
                        .then(function (store) { return res.status(200).json(store); })
                        .catch(function (err) { return res.status(404).json({ err: err }); })];
            case 1:
                _b.sent();
                return [2 /*return*/];
        }
    });
}); };
exports.deleteOrder = function (req, res, next) {
    var orderId = req.params.orderId;
    typeorm_1.getRepository(Order_entity_1.Order).delete(orderId)
        .then(function () { return res.status(200).json({ message: "Target earesed" }); })
        .catch(function (err) { return console.log(err); });
};
exports.getPurchaseOrderByDate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd, dateBetween, valueBetween;
    return __generator(this, function (_b) {
        _a = req.params, yearStart = _a.yearStart, monthStart = _a.monthStart, dayStart = _a.dayStart, yearEnd = _a.yearEnd, monthEnd = _a.monthEnd, dayEnd = _a.dayEnd;
        dateBetween = function (dateStart, dateEnd) { return typeorm_1.Between(dateStart, dateEnd); };
        valueBetween = dateBetween((new Date(Number(yearStart), Number(monthStart) - 1, Number(dayStart))), new Date(Number(yearEnd), Number(monthEnd) - 1, Number(dayEnd)));
        typeorm_1.getRepository(Order_entity_1.Order).find({
            relations: ["orderGenerator", "store", "client"],
            where: [
                { typeOrder: Order_entity_1.TypeOrder.PURCHASE, create_at: valueBetween }
            ],
            order: { status: 1, create_at: 1, create_colum_at: 1 }
        })
            .then(function (order) { return res.status(200).json(order); })
            .catch(function (err) { return res.status(404).json({ err: err }); });
        return [2 /*return*/];
    });
}); };
exports.getSalesOrderByDate = function (req, res, next) { return __awaiter(void 0, void 0, void 0, function () {
    var _a, yearStart, monthStart, dayStart, yearEnd, monthEnd, dayEnd, dateBetween, valueBetween;
    return __generator(this, function (_b) {
        _a = req.params, yearStart = _a.yearStart, monthStart = _a.monthStart, dayStart = _a.dayStart, yearEnd = _a.yearEnd, monthEnd = _a.monthEnd, dayEnd = _a.dayEnd;
        dateBetween = function (dateStart, dateEnd) { return typeorm_1.Between(dateStart, dateEnd); };
        valueBetween = dateBetween((new Date(Number(yearStart), Number(monthStart) - 1, Number(dayStart))), new Date(Number(yearEnd), Number(monthEnd) - 1, Number(dayEnd)));
        typeorm_1.getRepository(Order_entity_1.Order).find({
            relations: ["orderGenerator", "store", "client"],
            where: [
                { typeOrder: Order_entity_1.TypeOrder.SALES, create_at: valueBetween }
            ],
            order: { status: 1, create_at: 1, create_colum_at: 1 }
        })
            .then(function (order) { return res.status(200).json(order); })
            .catch(function (err) { return res.status(404).json({ err: err }); });
        return [2 /*return*/];
    });
}); };
