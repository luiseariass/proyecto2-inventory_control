"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.permissionSuperAdmin = exports.permissionAdmin = exports.authGlobal = exports.accessAPI = exports.isActive = exports.isLoggedIn = void 0;
exports.isLoggedIn = function (req, res, next) {
    if (req.session.passport) {
        if (req.session.passport.user) {
            next();
        }
        else {
            res.redirect('/login');
        }
    }
    else {
        res.redirect('/login');
    }
};
exports.isActive = function (req, res, next) {
    if (req.user) {
        if (req.user.status) {
            next();
        }
        else {
            res.render('auth/confirmation', { firstMail: false });
        }
    }
    else {
        res.redirect('/login');
    }
};
exports.accessAPI = function (req, res, next) {
    if (req.user && req.session.passport) {
        if (req.session.passport.user && req.user.status) {
            console.log("pasando por aqui");
            next();
        }
        else {
            res.status(401).json({ err: "Unauthorized" });
        }
    }
    else {
        res.status(401).json({ err: "Unauthorized" });
    }
};
exports.authGlobal = function (req, res, next) {
    if (req.session.passport) {
        if (req.session.passport.user) {
            req.app.locals.name = req.user.name;
            req.app.locals.logged = true;
            if (req.user.role === "SuperAdmin") {
                req.app.locals.roleSP = true;
                req.app.locals.roleAdmin = false;
                req.app.locals.roleRegular = false;
            }
            if (req.user.role === "Admin") {
                req.app.locals.roleSP = false;
                req.app.locals.roleAdmin = true;
                req.app.locals.roleRegular = false;
            }
            if (req.user.role === "Regular") {
                req.app.locals.roleSP = false;
                req.app.locals.roleAdmin = false;
                req.app.locals.roleRegular = true;
            }
        }
        else {
            req.app.locals.logged = false;
        }
    }
    else {
        req.app.locals.logged = false;
    }
    next();
};
exports.permissionAdmin = function (req, res, next) {
    if (req.session.passport) {
        if (req.session.passport.user) {
            if (req.user.role === "SuperAdmin" || req.user.role === "Admin") {
                next();
            }
            else {
                res.redirect('/index');
            }
        }
    }
};
exports.permissionSuperAdmin = function (req, res, next) {
    if (req.session.passport) {
        if (req.session.passport.user) {
            if (req.user.role === "SuperAdmin") {
                next();
            }
            else {
                res.redirect('/index');
            }
        }
    }
};
