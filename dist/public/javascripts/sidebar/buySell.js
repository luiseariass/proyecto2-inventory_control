const crudAPI = new APIHandler(`${document.location.origin}/api`);
let countProduct = [0]; 
console.log(document.location.origin)
async function clikedIdOrder(id,typeOfOrder) {
    await crudAPI.getOneOrder(id).then((order)=>{
        document.querySelector("#div-complet-date").style.display = "none"
        document.querySelector("#container-proof-of-payment").style.display = "block"
        document.querySelector("#back-order-buy").style.display = "block"
        document.querySelector("#back-order-sell").style.display = "block"
        document.querySelector("#container-order-pdf").style.display = "block"
        let tableOrder = document.querySelector(".table-product-buy")
        tableOrder.caption.innerHTML = `Compra del dia ${order.create_at.substr(0,10)}`
        tableOrder.tHead.innerHTML = '<tr> <th scope="col">Material</th> <th scope="col">Cantidad</th> <th scope="col">Precio</th> </tr>'
        document.querySelector(".title-tables").innerHTML = `Orden ${id}`
        let containerInfoProduct = document.querySelector(".container-info-product")
        let divInfoProduct = containerInfoProduct.querySelectorAll(":scope > div");
        if (typeOfOrder==="buy"){
            divInfoProduct[0].innerHTML = `Proveedor: ${order.client.name}`
        }else{
            divInfoProduct[0].innerHTML = `Cliente: ${order.client.name}`
        }
        divInfoProduct[1].innerHTML = `Monto:  ${Number(order.amount).toFixed(2)} </span> <span>$</span>` 
        divInfoProduct[2].innerHTML = `<span> Estado: ${order.status}</span> <span>`
        let stingTableTBody = ""
        for (let i=0; i<order.product.length;i++){
            stingTableTBody += `<tr> <td>${order.product[i]}</td> <td>${order.quantity[i]}</td> <td>${order.price[i]} <span> $ </span> </td> </tr>`
        }
        tableOrder.tBodies[0].innerHTML = stingTableTBody
        document.querySelector("#id-hidden-input").value = document.querySelector('.title-tables').innerHTML.substr(6)
        document.querySelector('.update-order-sell').addEventListener('click', async function (event) {
            let selectOrder =document.querySelector("#status-order").value
           await crudAPI.updateStatusOrder(document.querySelector('.title-tables').innerHTML.substr(6),selectOrder,"Sales Order").then((message)=>{
                updateView("order","sell")
           })
        });
        document.querySelector('.update-order-buy').addEventListener('click', async function (event) {
            let selectOrder =document.querySelector("#status-order").value
            await crudAPI.updateStatusOrder(document.querySelector('.title-tables').innerHTML.substr(6),selectOrder,"Purcharse Order").then((message)=>{
                 updateView("order","buy")
            })
         });
   })
   .catch((error)=>console.log(error))
  }
function createTableViewsProduct (table,ofTheDate,start,end,typeOfTable,typeOfOrder){
    if (typeOfOrder==="sell"){
        document.querySelector(".title-tables").innerHTML = "Productos Vendidos"
        table.caption.innerHTML = `Ventas para el periodo ${start} hasta ${end}`
    }else{
        document.querySelector(".title-tables").innerHTML = "Productos Comprados"
        table.caption.innerHTML = `Compras para el periodo ${start} hasta ${end}`
    }
    table.tHead.innerHTML = '<tr> <th scope="col">Material</th> <th scope="col">Cantidad</th> <th scope="col">Precio Promedio</th> </tr>'
    if (typeOfTable === "provider"){
        table.tHead.innerHTML = '<tr> <th scope="col">Proveedor</th> <th scope="col">Cantidad</th> <th scope="col">Precio Promedio</th> </tr>'   
    }
    table.tBodies[0].innerHTML = ""
    let product = {"total":{
        "accumulated" : 0,
        "quantity" : 0,
        "unpay" : 0,
    }}
    if (typeOfTable === "order"){
        
        if (typeOfOrder==="sell"){
            document.querySelector(".title-tables").innerHTML = "Ordenes Vendidas"
            table.tHead.innerHTML = '<tr> <th scope="col">Codigo</th> <th scope="col">Cliente</th> <th scope="col">Estado</th> <th scope="col">Monto</th> </tr>'
        }else{
            document.querySelector(".title-tables").innerHTML = "Ordenes Compradas"
            table.tHead.innerHTML = '<tr> <th scope="col">Codigo</th> <th scope="col">Proveedor</th> <th scope="col">Estado</th> <th scope="col">Monto</th> </tr>'
        }
        table.caption.innerHTML = `Ordenes para el periodo ${start} hasta ${end}`
        
    }

    for (let i = 0 ;i<ofTheDate.length;i++){
        let stringTableOrder = ''
        if (typeOfTable === "order"){
            if(ofTheDate[i].status === "Pending to pay"){
                product["total"].unpay++ 
            }
            stringTableOrder = `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIdOrder('${ofTheDate[i].id}','${typeOfOrder}')">${ofTheDate[i].id}</button></td> <td>${ofTheDate[i].client.name}</td> <td>${ofTheDate[i].status}</td> <td>${Number(ofTheDate[i].amount).toFixed(2)} <span> $ </span> </td> </tr>`
            table.tBodies[0].innerHTML += stringTableOrder
        }else{
            for (let j = 0; j<ofTheDate[i].product.length; j++){
                if (typeOfTable === "provider"){
                    if(product[ofTheDate[i].client.name]) {
                        product[ofTheDate[i].client.name]['accumulated'] += Number(ofTheDate[i].price[j])*Number(ofTheDate[i].quantity[j])
                        product[ofTheDate[i].client.name]['quantity'] += Number(ofTheDate[i].quantity[j])
                    }else{
                        product[ofTheDate[i].client.name] = {}
                        product[ofTheDate[i].client.name]['accumulated'] = Number(ofTheDate[i].price[j])*Number(ofTheDate[i].quantity[j])
                        product[ofTheDate[i].client.name]['quantity'] = Number(ofTheDate[i].quantity[j])
                    }
                }else if (typeOfTable === "product"){
                    if(product[ofTheDate[i].product[j]]) {
                        product[ofTheDate[i].product[j]]['accumulated'] += Number(ofTheDate[i].price[j])*Number(ofTheDate[i].quantity[j])
                        product[ofTheDate[i].product[j]]['quantity'] += Number(ofTheDate[i].quantity[j])
                    }else{
                        product[ofTheDate[i].product[j]] = {}
                        product[ofTheDate[i].product[j]]['accumulated'] = Number(ofTheDate[i].price[j])*Number(ofTheDate[i].quantity[j])
                        product[ofTheDate[i].product[j]]['quantity'] = Number(ofTheDate[i].quantity[j])
                    }
                } 
                
                product["total"]["accumulated"] +=  Number(ofTheDate[i].price[j])*Number(ofTheDate[i].quantity[j])
                product["total"]["quantity"] += Number(ofTheDate[i].quantity[j])
            }
            let stingTableTBody = ""
            for (prod in product){
                if (prod!=="total"){
                stingTableTBody += `<tr> <td>${prod}</td> <td>${product[prod].quantity}</td> <td>${(product[prod].accumulated/product[prod].quantity).toFixed(2)} <span> $ </span> </td> </tr>`
                }else{
                lastTableTBody = `<tr> <td scope="col">Total</td> <td>${product[prod].quantity}</td> <td>${(product[prod].accumulated/product[prod].quantity).toFixed(2)} <span> $ </span> </td> </tr>`
                let containerInfoProduct = document.querySelector(".container-info-product")
                let divInfoProduct = containerInfoProduct.querySelectorAll(":scope > div");
                divInfoProduct[0].innerHTML = `Cantidad Total: ${product[prod].quantity}`
                divInfoProduct[1].innerHTML = `Precio Promedio: <span>${(product[prod].accumulated/product[prod].quantity).toFixed(2)}</span> <span>$</span>`
                divInfoProduct[2].innerHTML = `Monto Gastado:  ${product[prod].accumulated.toFixed(2)} </span> <span>$</span>`
                              
            }
            }
        table.tBodies[0].innerHTML = stingTableTBody + lastTableTBody
        }
    if (typeOfTable === "order"){
        let containerInfoProduct = document.querySelector(".container-info-product")
        let divInfoProduct = containerInfoProduct.querySelectorAll(":scope > div");
        divInfoProduct[0].innerHTML = `Cantidad de Órdenes: ${ofTheDate.length}` 
        if (typeOfOrder==="sell"){
            divInfoProduct[1].innerHTML = `Órdenes Cobradas: <span>${ofTheDate.length-product["total"].unpay}`
            divInfoProduct[2].innerHTML = `Órdenes por Cobrar:  ${product["total"].unpay}`              
        }else{
            divInfoProduct[1].innerHTML = `Órdenes Pagadas: <span>${ofTheDate.length-product["total"].unpay}`
            divInfoProduct[2].innerHTML = `Órdenes por Pagar:  ${product["total"].unpay}`}   
        } 
    }
}
async function getTableProduct(period,typeOfTable,typeOfOrder){
    let tableBuy = document.querySelector(".table-product-buy")
    
    let OfTheDate = await crudAPI.getBuyOfDate(period[0],period[1])
    if (typeOfOrder==="sell"){
        OfTheDate = await crudAPI.getSalesOfDate(period[0],period[1])
    }
    if (OfTheDate.length >0){
        createTableViewsProduct(tableBuy,OfTheDate,period[0],period[1],typeOfTable,typeOfOrder)
    }else{
        tableBuy.caption.innerHTML = `Sin compras para el periodo ${period[0]} hasta ${period[1]}`
        if (typeOfOrder==="sell"){
            tableBuy.caption.innerHTML = `Sin ventas para el periodo ${period[0]} hasta ${period[1]}`
        }
        
        tableBuy.tHead.innerHTML = ""
        tableBuy.tBodies[0].innerHTML = "" 
        let containerInfoProduct = document.querySelector(".container-info-product")
        let divInfoProduct = containerInfoProduct.querySelectorAll(":scope > div");
        divInfoProduct[0].innerHTML = `Cantidad Total: 0`
        divInfoProduct[1].innerHTML = `Precio Promedio: <span>0</span> <span>$</span>`
        divInfoProduct[2].innerHTML = `Monto Gastado:  0</span> <span>$</span>`

    }
}
async function updateView(typeOfTable,typeOfOrder="buy"){
    console.log(typeOfOrder)
    document.querySelector("#div-complet-date").style.display = "block"
    document.querySelector("#container-proof-of-payment").style.display = "none"
    document.querySelector(".container-prod").innerHTML= ""
    document.querySelector(".view-buy-product").style.display = "block"
    document.querySelector(".view-buy-create-orden").style.display="none"
    document.querySelector("#back-order-buy").style.display = "none"
    document.querySelector("#back-order-sell").style.display = "none"
    document.querySelector("#container-order-pdf").style.display = "none"
    let dateEnd = new Date()
    let dateStart = new Date(dateEnd.getUTCFullYear(),dateEnd.getMonth(),dateEnd.getDate()-7) 
    let dayStart = dateStart.getDate() < 10 ? '0'+dateStart.getDate() : dateStart.getDate()
    let monthStart =  dateStart.getMonth() + 1  < 10 ? '0'+ (dateStart.getMonth() + 1) : dateStart.getMonth() + 1
    let yearStart = dateStart.getUTCFullYear()
    let dayEnd = dateEnd.getDate() < 10 ? '0'+dateEnd.getDate() : dateEnd.getDate()
    let monthEnd =  dateEnd.getMonth() + 1  < 10 ? '0'+ (dateEnd.getMonth() + 1) : dateEnd.getMonth() + 1
    let yearEnd = dateEnd.getUTCFullYear()
    document.getElementById("end-buy-product").value = `${yearEnd}-${monthEnd}-${dayEnd}`
    document.getElementById("start-buy-product").value = `${yearStart}-${monthStart}-${dayStart}`
    getTableProduct([`${yearStart}-${monthStart}-${dayStart}`,`${yearEnd}-${monthEnd}-${dayEnd}`],typeOfTable,typeOfOrder)
    document.getElementById('search-input-buy-product').addEventListener('click', function (event) {
        event.preventDefault()
        dateStart = document.getElementById("start-buy-product").value
        dateEnd = document.getElementById("end-buy-product").value
        getTableProduct([dateStart,dateEnd],typeOfTable,typeOfOrder)
    }); 
      
}
async function updateViewCreateOrder(){
    document.querySelector(".view-buy-create-orden").style.display="block"
    document.querySelector(".view-buy-product").style.display = "none"
    document.querySelector("#back-order-buy").style.display = "none"
    document.querySelector("#back-order-sell").style.display = "none"
    document.querySelector("#store-id").innerHTML = ""
    document.querySelector("#client-id").innerHTML = ""
    document.querySelector(".container-prod").innerHTML= ""
    document.querySelector("#container-order-pdf").style.display = "none"
    let store = document.querySelector("#store-id")
    let provider = document.querySelector("#client-id")
    const date = new Date 
    let month = date.getMonth() + 1;
    let day = date.getDate(); 
    if (day<10){
        day = `0${day}`
    }
    if (month<10){
        month = `0${month}`
    } 
    document.querySelector("#date-order").value = `${date.getUTCFullYear()}-${month}-${day}`
    const allStores = await crudAPI.getAllStore()
    .then((data)=> {
        for (let i = 0; i<data["Store"].length;i++){
            let option = document.createElement("option");
            option.text=data["Store"][i].name
            option.value =  data["Store"][i].id
            store.add(option)
        }
    })
    .catch()
    const allProvider = await crudAPI.getAllClient()
    .then((data)=> {
        for (let i = 0; i<data["client"].length;i++){
            let option = document.createElement("option");
            option.text=data["client"][i].name
            option.value =  data["client"][i].id
            provider.add(option)
        }
    })
    .catch()
    
    let product = ["Hard Aluminum","Aluminum Cans","Mixt Aluminum","Copper","Bronze","Steel"]
    document.querySelector('.plus-product').addEventListener('click', function (event) {
      event.preventDefault()
      event.stopImmediatePropagation();
      if (countProduct[0] === 0){
        document.querySelector(".container-prod").innerHTML= ""
      }
      console.log(countProduct[0]) 
      console.log(document.querySelectorAll(".product-price-quantity").length)
      let containerProd = document.querySelector(".container-prod")
      if (countProduct[0]<=product.length-1){
        countProduct[0]++
        let divRow = document.createElement("div")
        divRow.classList.add("row");
        let divContainerMaterial = document.createElement("div")
        divContainerMaterial.classList.add("col-3");
        divContainerMaterial.classList.add("product-price-quantity");
        let selectContainer = document.createElement("select")
        selectContainer.classList.add("product-select");
        selectContainer.name = "product"
        let labelProduct = document.createElement("label")
        labelProduct.innerHTML = "Material:"
        labelProduct.htmlFor = "product"
        divContainerMaterial.appendChild(labelProduct)
        for (let i=0; i<product.length;i++){
            let optionProduct = document.createElement("option")
            optionProduct.text=(product[i])
            selectContainer.add(optionProduct)
        }
        divContainerMaterial.appendChild(selectContainer)


        let divContainerPrice = document.createElement("div")
        divContainerPrice.classList.add("col-3");
        divContainerPrice.classList.add("product-price-quantity");
        let inputPrice = document.createElement("input")
        inputPrice.setAttribute("type","number")
        inputPrice.min = 0
        inputPrice.step = 0.1
        inputPrice.classList.add("price-select");
        inputPrice.name = "Price"
        let labelPrice = document.createElement("label")
        labelPrice.innerHTML = "Precio:"
        labelPrice.htmlFor = "Price"
        divContainerPrice.appendChild(labelPrice)
        divContainerPrice.appendChild(inputPrice)

        let divContainerQuantity = document.createElement("div")
        divContainerQuantity.classList.add("col-4");
        divContainerQuantity.classList.add("product-price-quantity");
        let inputQuantity = document.createElement("input")
        inputQuantity.setAttribute("type","number")
        inputQuantity.min = 0
        inputQuantity.classList.add("quantity-select");
        inputQuantity.name = "Quantity"
        let labelQuantity = document.createElement("label")
        labelQuantity.innerHTML = "Cantidad:"
        labelQuantity.htmlFor = "Quantity"
        divContainerQuantity.appendChild(labelQuantity)
        divContainerQuantity.appendChild(inputQuantity)

        divRow.appendChild(divContainerMaterial)
        divRow.appendChild(divContainerPrice)
        divRow.appendChild(divContainerQuantity)     
        containerProd.appendChild(divRow)
      } 
      

      
    });
    document.querySelector('.create-order-buy').addEventListener('click', async function (event) {
        event.stopImmediatePropagation();
        event.preventDefault()
      if (countProduct>0){
        let documentAllProduct = document.querySelectorAll(".product-select")
        let documentAllPrice = document.querySelectorAll(".price-select")
        let documentAllQuantity = document.querySelectorAll(".quantity-select")
        let allProduct = []
        let allPrice = []
        let allQuantity = []
        for (let i=0; i<countProduct;i++){
            allProduct.push(documentAllProduct[i].value)
            allPrice.push(documentAllPrice[i].value)
            allQuantity.push(documentAllQuantity[i].value)
        }
        let create_at = document.querySelector("#date-order").value
        create_at += " 00:00:00"
        store = document.querySelector("#store-id").value
        provider = document.querySelector("#client-id").value
        let unique = [...new Set(allProduct)];
        if (unique.length === allProduct.length && !allQuantity.find(quan => quan === 0)){
            await crudAPI.createOrder(allProduct,provider,store,"Purcharse Order",allPrice,allQuantity,create_at).then((result)=>{
                document.querySelector(".container-prod").innerHTML = ""
                countProduct[0]=0
            }).catch((error)=>console.log(error))
        }else{
            alert("Los Materiales tienen que ser distintos y las cantidades no pueden ser cero");
        } 
      }else{
        alert("No puede crear una orden vacia");
      }
    });
    document.querySelector('.create-order-sell').addEventListener('click', async function (event) {
        event.preventDefault()
        event.stopImmediatePropagation();
        if (countProduct>0){
          let documentAllProduct = document.querySelectorAll(".product-select")
          let documentAllPrice = document.querySelectorAll(".price-select")
          let documentAllQuantity = document.querySelectorAll(".quantity-select")
          let allProduct = []
          let allPrice = []
          let allQuantity = []
          for (let i=0; i<countProduct;i++){
              allProduct.push(documentAllProduct[i].value)
              allPrice.push(documentAllPrice[i].value)
              allQuantity.push(documentAllQuantity[i].value)
          }
          let create_at = document.querySelector("#date-order").value
          create_at += " 00:00:00"
          store = document.querySelector("#store-id").value
          client = document.querySelector("#client-id").value
          let unique = [...new Set(allProduct)];
          if (unique.length === allProduct.length && !allQuantity.find(quan => quan === 0)){
            await crudAPI.createOrder(allProduct,client,store,"Sales Order",allPrice,allQuantity,create_at).then((result)=>{
              document.querySelector(".container-prod").innerHTML = ""
              countProduct[0]=0
          }).catch((error)=>console.log(error))
        }else{
            alert("Los Materiales tienen que ser distintos y las cantidades no pueden ser cero");
        } 
        }else{
            alert("No puede crear una orden vacia");}
      });

}
window.addEventListener('load', () => {
    if(document.querySelector("#whereAreFrom").innerHTML === "sell"){
        updateView("order","sell")
    }else if (document.querySelector("#whereAreFrom").innerHTML === "buy"){
        updateView("order")
    }else{
        updateViewCreateOrder()
    }
    
    document.querySelector('.create-view-buy').addEventListener('click', function (event) {
        event.preventDefault()
        countProduct = [0]
        updateViewCreateOrder()
    });
    document.querySelector('.product-view-buy').addEventListener('click', function (event) {
        countProduct = [0]
        event.preventDefault()
        updateView("product")
    });
    document.querySelector('.product-view-sell').addEventListener('click', function (event) {
        countProduct = [0]
        event.preventDefault()
        updateView("product","sell")
    });
    document.querySelector('.provider-view-buy').addEventListener('click', function (event) {
        countProduct = [0]
        event.preventDefault()
        updateView("provider")
    });
    document.querySelector('.provider-view-sell').addEventListener('click', function (event) {
        countProduct = [0]
        event.preventDefault()
        updateView("provider","sell")
    });
    document.querySelector('.order-view-buy').addEventListener('click', async function (event) {
        event.preventDefault()
        updateView("order")
    });
    document.querySelector('.order-view-sell').addEventListener('click', async function (event) {
        event.preventDefault()
        updateView("order","sell")
    });
    document.querySelector('#back-order-buy').addEventListener('click', async function (event) {
        event.preventDefault()
        updateView("order")
    });
    
    document.querySelector('#back-order-sell').addEventListener('click', async function (event) {
        event.preventDefault()
        updateView("order","sell")
    });
})


