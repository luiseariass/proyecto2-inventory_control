const crudAPI = new APIHandler(`${document.location.origin}/api`);

async function updateInventory(){
    let table = document.querySelector(".table-inventory")
    table.caption.innerHTML = `Inventario`
    table.tHead.innerHTML = '<tr> <th scope="col">Almacen</th> <th scope="col">Cantidad</th> <th scope="col">Monto de Inventario</th> </tr>'
    table.tBodies[0].innerHTML = ""
    document.querySelector("#back-inventory").style.display = "none"
    const allStores = await crudAPI.getAllStore()
        .then((data)=> {
            let totalAmount = 0;
            let totalQuantity =0;
            for (let i = 0; i<data["Store"].length;i++){
                let quantity = 0;
                let amount = 0;
                let product = {
                    "Hard Aluminum" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        }
                    },
                    "Aluminum Cans" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        } 
                    },
                    "Mixt Aluminum" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        } 
                    },
                    "Copper" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        } 
                    },
                    "Bronze" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        } 
                    },
                    "Steel" : {
                        sell : 0, 
                        buy  : {
                            amount : 0,
                            quantity : 0,
                            price : 0,
                        } 
                    }
                }
                for (j=0;j<data["Store"][i].order.length;j++){
                    if(data["Store"][i].order.length>0){
                        if (data["Store"][i].order[j].typeOrder === "Sales Order"){
                            quantity-= data["Store"][i].order[j].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0) ;
                            for (let k=0;k<data["Store"][i].order[j].product.length;k++){

                                product[data["Store"][i].order[j].product[k]].sell += Number(data["Store"][i].order[j].quantity[k])
                            }
                        }else{
                            quantity+= data["Store"][i].order[j].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0) ;
                            for (let k=0;k<data["Store"][i].order[j].product.length;k++){
                                product[data["Store"][i].order[j].product[k]].buy.quantity += Number(data["Store"][i].order[j].quantity[k])
                                product[data["Store"][i].order[j].product[k]].buy.amount += Number(data["Store"][i].order[j].quantity[k]) * Number(data["Store"][i].order[j].price[k])
                                product[data["Store"][i].order[j].product[k]].buy.price = product[data["Store"][i].order[j].product[k]].buy.amount/ product[data["Store"][i].order[j].product[k]].buy.quantity
                            }
                        }
                    }
                    
                }
                for (prod in product){
                    amount += product[prod].buy.amount - product[prod].sell*product[prod].buy.price  
                }
                totalAmount+=amount;
                totalQuantity+=quantity;
                table.tBodies[0].innerHTML += `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIds('${data["Store"][i].id}')">${data["Store"][i].name}</button></td> <td>${quantity}</td> <td>${amount.toFixed(2)} <span>$</span></td> </tr>`
            }
            let containerInfoInventory = document.querySelector(".container-info-inventory")
            let divInfoInventory = containerInfoInventory.querySelectorAll(":scope > div");
            divInfoInventory[0].innerHTML = `Cantidad Total de Materiales: ${totalQuantity}`
            divInfoInventory[1].innerHTML = `Monto de Inventario: <span>${(totalAmount).toFixed(2)}</span> <span>$</span>`
        }).catch()
}

async function clikedIds(id) {
    await crudAPI.getOneStore(id).then((store)=>{
        let table = document.querySelector(".table-inventory")
        table.tHead.innerHTML = '<tr> <th scope="col">Material</th> <th scope="col">Cantidad</th> <th scope="col">Monto de Inventario</th> </tr>'
        table.caption.innerHTML = `Inventario del almacen ${store.name}`
        table.tBodies[0].innerHTML = ""
        let quantity = 0;
        let amount = 0;
        let product = {
            "Hard Aluminum" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                }
            },
            "Aluminum Cans" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                } 
            },
            "Mixt Aluminum" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                } 
            },
            "Copper" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                } 
            },
            "Bronze" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                } 
            },
            "Steel" : {
                sell : 0, 
                buy  : {
                    amount : 0,
                    quantity : 0,
                    price : 0,
                } 
            }
        }
        for (j=0;j<store.order.length;j++){
            if(store.order.length>0){
                if (store.order[j].typeOrder === "Sales Order"){
                    quantity-= store.order[j].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0) ;
                    for (let k=0;k<store.order[j].product.length;k++){

                        product[store.order[j].product[k]].sell += Number(store.order[j].quantity[k])
                    }
                }else{
                    quantity+= store.order[j].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0) ;
                    for (let k=0;k<store.order[j].product.length;k++){
                        product[store.order[j].product[k]].buy.quantity += Number(store.order[j].quantity[k])
                        product[store.order[j].product[k]].buy.amount += Number(store.order[j].quantity[k]) * Number(store.order[j].price[k])
                        product[store.order[j].product[k]].buy.price = product[store.order[j].product[k]].buy.amount/ product[store.order[j].product[k]].buy.quantity
                    }
                }
            }
            
        }
        let materialQuantity = 0;
        let materialAmount = 0;
        for (prod in product){
            amount += product[prod].buy.amount - product[prod].sell*product[prod].buy.price  
            materialQuantity =  product[prod].buy.quantity - product[prod].sell
            materialAmount = product[prod].buy.amount - product[prod].sell*product[prod].buy.price 
            table.tBodies[0].innerHTML += `<tr> <td>${prod}</td> <td>${materialQuantity}</td> <td>${materialAmount.toFixed(2)} <span>$</span></td> </tr>`
        }
        let containerInfoInventory = document.querySelector(".container-info-inventory")
        let divInfoInventory = containerInfoInventory.querySelectorAll(":scope > div");
        divInfoInventory[0].innerHTML = `Cantidad Total de Materiales: ${quantity}`
        divInfoInventory[1].innerHTML = `Monto de Inventario: <span>${(amount).toFixed(2)}</span> <span>$</span>`
        document.querySelector("#back-inventory").style.display = "block"

            
    })
    .catch((error)=>console.log(error))
}   

window.addEventListener('load', () => {
    updateInventory()
    document.getElementById('back-inventory').addEventListener('click', function (event) {
      event.preventDefault()
      updateInventory()
    });
})


