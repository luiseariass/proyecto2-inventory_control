class APIHandler {
    constructor(baseUrl){
        this.BASE_URL =baseUrl
    }
    async getAllUser(){
        try {
          const user = await axios.get(`${this.BASE_URL}/user/all`)
          console.log(user.data)
          return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async getBuyOfTheDay(){
        try {
            let date = new Date()
            let day = date.getDate();
            let month = date.getMonth() + 1; 
            let year = date.getFullYear();
            const user = await axios.get(`${this.BASE_URL}/order/purchaseorder/date/${year}/${month}/${day}&${year}/${month}/${day}`)
            return(user.data)
          } catch (error) {
                console.log(error)
                return(error)
          }
    }
    async getSalesOfTheDay(){
        try {
            let date = new Date()
            let day = date.getDate();
            let month = date.getMonth() + 1; 
            let year = date.getFullYear();
            const user = await axios.get(`${this.BASE_URL}/order/salesorder/date/${year}/${month}/${day}&${year}/${month}/${day}`)
            return(user.data)
          } catch (error) {
                console.log(error)
                return(error)
          }
    }
    async getBuyOfDate(start,end){
        try {
            let dateStart = start.split("-")
            let dateEnd = end.split("-")
            const user = await axios.get(`${this.BASE_URL}/order/purchaseorder/date/${dateStart[0]}/${dateStart[1]}/${dateStart[2]}&${dateEnd[0]}/${dateEnd[1]}/${dateEnd[2]}`)
            return(user.data)
          } catch (error) {
                console.log(error)
                return(error)
          }
    }
    async getSalesOfDate(start,end){
        try {
            let dateStart = start.split("-")
            let dateEnd = end.split("-")
            const user = await axios.get(`${this.BASE_URL}/order/salesorder/date/${dateStart[0]}/${dateStart[1]}/${dateStart[2]}&${dateEnd[0]}/${dateEnd[1]}/${dateEnd[2]}`)
            return(user.data)
          } catch (error) {
                console.log(error)
                return(error)
          }
    }
    async getAllStore(){
        try {
          const user = await axios.get(`${this.BASE_URL}/store/all`)
          return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async getAllClient(){
        try {
          const user = await axios.get(`${this.BASE_URL}/client/all`)
          return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async createOrder(product,client,store,typeOrder,price,quantity,create_at){
        try {
            const user = await axios.post(`${this.BASE_URL}/order`,{product,client,store,typeOrder,price,quantity,create_at})
            console.log(user)
            return(user)
          } catch (error) {
              console.log(error)
            return(error)
          }

    }
    async getOneOrder(id){
        try {
            console.log(id)
            const order = await axios.get(`${this.BASE_URL}/order/${id}`)
            console.log(order)
            return(order.data)
          } catch (error) {
              console.log(error)
            return(error)
          }

    }
    async getOneStore(id){
        try {
            console.log(id)
            const store = await axios.get(`${this.BASE_URL}/store/${id}`)
            return(store.data)
          } catch (error) {
              console.log(error)
            return(error)
          }
    }
    async getOneUser(id){
        try {
            console.log(id)
            const user = await axios.get(`${this.BASE_URL}/user/${id}`)
            return(user.data)
          } catch (error) {
              console.log(error)
            return(error)
          }
    }
    async updateStatusOrder(id,status,typeOfOrder){
        try {
            const order = await axios.put(`${this.BASE_URL}/order/update`,{id, status, typeOfOrder})
            console.log(order)
            return(order.data)
          } catch (error) {
              console.log(error)
            return(error)
          }
    }
    async getAllUserInactive(){
        try {
          const user = await axios.get(`${this.BASE_URL}/user/all/active/false`)
          return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async getAllUserActive(){
        try {
          const user = await axios.get(`${this.BASE_URL}/user/all/active/true`)
          return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async updateUser(id, role, status,store,email,name){
        try {
            console.log(id,role, status,store,email,name)
            const user = await axios.put(`${this.BASE_URL}/user/${id}`,{"role":role, "status" : status, "store": store, "email" : email, "name" : name})
            return(user.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async createClient(name, email,bank,clabe){
        try {
            const client = await axios.post(`${this.BASE_URL}/client`,{name,email,bank,clabe})
            return(client.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async getAllClient(){
        try {
            const client = await axios.get(`${this.BASE_URL}/client/all`)
            return(client.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async updateClient(id,name,email,bank,clabe){
        try {
            const client = await axios.put(`${this.BASE_URL}/client/${id}`,{name,email,bank,clabe})
            return(client.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async createStore(name,address, longitude,latitude){
        try {
            const client = await axios.post(`${this.BASE_URL}/store`,{name,address, longitude,latitude})
            return(client.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
    async updateStore(id,name,address, longitude,latitude){
        console.log("id: 3", id)
        console.log(`${this.BASE_URL}/store/${id}`)
        try {
            const client = await axios.put(`${this.BASE_URL}/store/${id}`,{name,address, longitude,latitude})
            return(client.data)
        } catch (error) {
            console.log(error)
          return(error)
        }
    }
}