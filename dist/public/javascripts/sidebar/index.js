const crudAPI = new APIHandler(`${document.location.origin}/api`);
function createTable (table,option,ofTheDay){
        table.caption.innerHTML = option === 1 ? "Ventas del dia" : "Compras del dia"
        table.tHead.innerHTML = '<tr> <th scope="col">Material</th> <th scope="col">Cantidad</th> <th scope="col">Precio Promedio</th> </tr>'
        let product = {"total":{
            "accumulated" : 0,
            "quantity" : 0
        }}
        for (let i = 0 ;i<ofTheDay.length;i++){
            for (let j = 0; j<ofTheDay[i].product.length; j++){
                if(product[ofTheDay[i].product[j]]) {
                    product[ofTheDay[i].product[j]]['accumulated'] += Number(ofTheDay[i].price[j])*Number(ofTheDay[i].quantity[j])
                    product[ofTheDay[i].product[j]]['quantity'] += Number(ofTheDay[i].quantity[j])
                }else{
                    product[ofTheDay[i].product[j]] = {}
                    product[ofTheDay[i].product[j]]['accumulated'] = Number(ofTheDay[i].price[j])*Number(ofTheDay[i].quantity[j])
                    product[ofTheDay[i].product[j]]['quantity'] = Number(ofTheDay[i].quantity[j])
                }
                product["total"]["accumulated"] +=  Number(ofTheDay[i].price[j])*Number(ofTheDay[i].quantity[j])
                product["total"]["quantity"] += Number(ofTheDay[i].quantity[j])
            }
        }
        let stingTableTBody = ""
        for (prod in product){
            if (prod!=="total"){
                stingTableTBody += `<tr> <td>${prod}</td> <td>${product[prod].quantity}</td> <td>${(product[prod].accumulated/product[prod].quantity).toFixed(2)} <span> $ </span> </td> </tr>`
            }else{
                lastTableTBody = `<tr> <td scope="col">Total</td> <td>${product[prod].quantity}</td> <td>${(product[prod].accumulated/product[prod].quantity).toFixed(2)} <span> $ </span> </td> </tr>`
            }
            }
        table.tBodies[0].innerHTML = stingTableTBody + lastTableTBody
}
async function getTable(){
    const tableBuy = document.querySelector(".table-buy")
    const tableSell = document.querySelector(".table-sell")
    buyOfTheDay = await crudAPI.getBuyOfTheDay()
    salesOfTheDay = await crudAPI.getSalesOfTheDay()
    console.log(buyOfTheDay.length)
    if (buyOfTheDay.length >0){
        createTable(tableBuy,0,buyOfTheDay)
    }else{
        tableBuy.caption.innerHTML = "Sin compras para el periodo "
        tableBuy.tHead.innerHTML = ""
        tableBuy.tBodies[0].innerHTML = "" 

    }
    if (salesOfTheDay.length > 0){
        createTable(tableSell,1,salesOfTheDay)
    }else{
        tableSell.caption.innerHTML = "Sin ventas el dia de hoy"
        tableSell.tHead.innerHTML = ""
        tableSell.tBodies[0].innerHTML = "" 
    }
}

getTable()
window.onload = () => {
    getTable()
    //interval = setInterval(getTable, 10000 / 60);
}
 
  
  
  
  


