const crudAPI = new APIHandler(`${document.location.origin}/api`);
//poner la fecha del dia de cuando se hace la llamada del servicio.
const date = new Date 
let month = date.getMonth() + 1;
let day = date.getDate(); 
if (day<10){
    day = `0${day}`
}
if (month<10){
    month = `0${month}`
} 

let dateStartSell = `${date.getUTCFullYear()}-01-01`
document.getElementById("start-sell").value = dateStartSell
let dateStartBuy = `${date.getUTCFullYear()}-01-01`
document.getElementById("start-buy").value = dateStartBuy


let dateEndSell = `${date.getUTCFullYear()}-${month}-${day}`
document.getElementById("end-sell").value = dateEndSell
let dateEndBuy = `${date.getUTCFullYear()}-${month}-${day}`
document.getElementById("end-buy").value = dateEndBuy


const ctxSell = document.getElementById('chartSell').getContext('2d');
const ctxBuy = document.getElementById('chartBuy').getContext('2d');
function selectPeriod(string){
    let newDate = ''
    let dateStart = ""
    if(string==="week"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-7)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    }else if (string === "twoWeek"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-14)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    }else if (string === "threeWeek"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-21)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 

    }else if (string === "oneMonth"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-30)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    }else if (string === "threeMonth"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-90)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    }else if (string === "SixMonth"){
        newDate = new Date (date.getUTCFullYear(),month-1,day-180)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    }else{
        newDate = new Date (date.getUTCFullYear(),month-1,day-365)
        dateStart = `${newDate.getUTCFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`
        return dateStart 
    } 
    
}
function showChart (ctx, label,point,title) {
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: label,
            datasets: [{
                label: title,
                data: point,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)'
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
}
async function updateChart (start,end,option) {
    let data = ''
    let title = `Ventas para el periodo ${start} hasta ${end}`
    let ctx = ctxSell
    let updateInfo = ''
    let action = ["Vender","cobradas","cobrar"] 
    if (option==="sell"){
        data = await crudAPI.getSalesOfDate(start,end)
        updateInfo = document.querySelector(".only-sell")
        
        
    }else{
        data = await crudAPI.getBuyOfDate(start,end)
        title = `Compras para el periodo ${start} hasta ${end}`
        ctx = ctxBuy
        updateInfo = document.querySelector(".only-buy")
        action = ["Comprar","pagadas","pagar"]
    }
    order = {
        status : {
            pending : {
                amount : 0,
                sum : 0
            },
            pay : {
                amount : 0,
                sum : 0
            },
            total : {
                quantity:0,
                amount : 0,
                sum : 0
            },
        },
       date : {} 
    }
    for (let i = 0; i<data.length; i++){
        let splitData = data[i].create_at.split("-")
        let key = splitData[0]+"-"+splitData[1]+"-"+splitData[2].substr(0,2)
        if (order.date[key]){
            order.date[key] += data[i].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0)
        }else{
            order.date[key]={}
            order.date[key]= data[i].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0)
        }
        if (data[i].status === "Pending to pay"){
            order.status.pending.amount += Number(data[i].amount)
            order.status.pending.sum +=1
        }else{
            order.status.pay.amount += Number(data[i].amount)
            order.status.pay.sum +=1
        }
        order.status.total.quantity +=data[i].quantity.reduce(function(acc, val) { return acc + Number(val); }, 0)
        order.status.total.amount+=Number(data[i].amount)
        order.status.total.sum +=1
    }
   
    showChart(ctx,Object.keys(order.date),Object.values(order.date),title)
    let divInfo = updateInfo.querySelectorAll(":scope > div");
    divInfo[0].innerHTML = `Numero de ${action[0]}: ${order.status.total.sum}`
    divInfo[1].innerHTML = `Valor de ${action[0]}: <span>${(order.status.total.amount).toFixed(2)}</span> <span>$</span>`
    divInfo[2].innerHTML = `Numero de ${action[0]} ${action[1]}: ${order.status.pay.sum}`
    divInfo[3].innerHTML = `Valor de ${action[0]} ${action[1]}: <span>${(order.status.pay.amount).toFixed(2)}</span> <span>$</span>`
    divInfo[4].innerHTML = `Numero de ${action[0]} por ${action[2]}: ${order.status.pending.sum}`
    divInfo[5].innerHTML = `Valor de ${action[0]} por ${action[2]}: <span>${(order.status.pending.amount).toFixed(2)}</span> <span>$</span>`
}

updateChart(dateStartBuy,dateEndBuy,"buy")
updateChart(dateStartSell,dateEndSell,"sell")
function periodSelectBuy() {  
    let period = document.getElementById("period-id-buy").value;
    let dateStart = selectPeriod(period)
    let dateEnd = `${date.getUTCFullYear()}-${month}-${day}`
    updateChart(dateStart,dateEnd,"buy") 
}

function periodSelectSell() {  
    let period = document.getElementById("period-id-sell").value;
    let dateStart = selectPeriod(period)
    let dateEnd = `${date.getUTCFullYear()}-${month}-${day}`
    updateChart(dateStart,dateEnd,"sell") 
}

window.addEventListener('load', () => {
    document.getElementById('search-input-buy').addEventListener('click', function (event) {
      event.preventDefault()
      dateStart = document.getElementById("start-buy").value
      dateEnd = document.getElementById("end-buy").value
      updateChart(dateStart,dateEnd,"buy") 
    });
    document.getElementById('search-input-sell').addEventListener('click', function (event) {
        event.preventDefault()
        dateStart = document.getElementById("start-sell").value
        dateEnd = document.getElementById("end-sell").value
        updateChart(dateStart,dateEnd,"sell") 
      });
})



