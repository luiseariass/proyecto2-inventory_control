const crudAPI = new APIHandler(`${document.location.origin}/api`);
async function getViewCreateStore(){
    document.querySelector("#back-create-store").style.display = "none"
    document.querySelector(".view-store-table").style.display = "none"
    document.querySelector(".view-create-store").style.display = "block"
    document.querySelector(".view-store").style.display = "block"
    document.querySelector(".update-store").style.display = "none"
    document.querySelector(".create-store").style.display = "block"
    document.querySelector(".status-title").innerHTML = `Crea una nuevo Almacen`
    document.querySelector('.create-store').addEventListener('click', async function (event) {
        event.stopImmediatePropagation();
        event.preventDefault()
        let nameStore = document.querySelector("#name-id").value
        let addressStore = document.querySelector("#address-id").value
        let latitudeStore = document.querySelector("#latitude-id").value
        let longitudeStore = document.querySelector("#longitude-id").value
        console.log(nameStore,addressStore,latitudeStore,longitudeStore)
        if (nameStore !== "" && addressStore !== ""){
            await crudAPI.createStore(nameStore,addressStore,longitudeStore,latitudeStore).then((result)=>{
                document.querySelector("#name-id").value = ""
                document.querySelector("#address-id").value = ""
                document.querySelector("#latitude-id").value = ""
                document.querySelector("#longitude-id").value = ""
                getViewStore()
            }).catch((error)=>console.log(error))
        }else{
            alert("Todos los campos deben estar llenos");
        } 
    });
}
async function createTableStore (){
    let table = document.querySelector(".table-store")
    document.querySelector(".title-tables").innerHTML = "Almacenes"
    table.caption.innerHTML = `Almacenes`
    table.tHead.innerHTML = '<tr> <th scope="col">Nombre</th> <th scope="col">Dirección</th> <th scope="col">ID</th> </tr>'
    table.tBodies[0].innerHTML = ""
    await crudAPI.getAllStore().then((resultstore)=>{
        let datastore =resultstore['Store'] 
        for (let i = 0 ;i<datastore.length;i++){
            let stringTableOrder = ''
            stringTableOrder = `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIdStore('${datastore[i].name}','${datastore[i].location.address}','${datastore[i].location.longitude}','${datastore[i].location.latitude}','${datastore[i].id}')">${datastore[i].name}</button></td> <td>${datastore[i].location.address}</td> <td>${datastore[i].id}</td> </tr>`
            table.tBodies[0].innerHTML += stringTableOrder
        }
    }).catch((error)=>console.log(error))
   
}
function clikedIdStore(name,address,longitude,latitude,id){
    document.querySelector("#back-create-store").style.display = "none"
    document.querySelector(".view-store-table").style.display = "none"
    document.querySelector(".view-create-store").style.display = "block"
    document.querySelector(".view-store").style.display = "block"
    document.querySelector(".update-store").style.display = "block"
    document.querySelector(".create-store").style.display = "none"
    document.querySelector("#name-id").value = name
    document.querySelector("#address-id").value = address
    document.querySelector("#latitude-id").value = longitude
    document.querySelector("#longitude-id").value = latitude
    document.querySelector("#id").value = id
    document.querySelector(".status-title").innerHTML = `Actualizar Almacen ${name}`
    document.querySelector('.update-store').addEventListener('click', async function (event) {
        event.stopImmediatePropagation();
        event.preventDefault()
        let nameStore = document.querySelector("#name-id").value
        let addressStore = document.querySelector("#address-id").value
        let latitudeStore = document.querySelector("#latitude-id").value
        let longitudeStore = document.querySelector("#longitude-id").value
        let idStores = document.querySelector("#id").value
        console.log(nameStore,addressStore,latitudeStore,longitudeStore)
        if (nameStore !== "" && addressStore !== ""){
            console.log("id: 2", idStores)
            await crudAPI.updateStore(idStores,nameStore,addressStore,longitudeStore,latitudeStore).then((result)=>{
                document.querySelector("#name-id").value = ""
                document.querySelector("#address-id").value = ""
                document.querySelector("#latitude-id").value = ""
                document.querySelector("#longitude-id").value = ""
                getViewStore()
            }).catch((error)=>console.log(error))
        }else{
            alert("Todos los campos deben estar llenos");
        } 
    });




}
function getViewStore(){
    document.querySelector("#back-create-store").style.display = "block"
    document.querySelector(".view-store-table").style.display = "block"
    document.querySelector(".view-create-store").style.display = "none"
    document.querySelector('.view-store').style.display = "none"
    createTableStore()
}

getViewCreateStore()
window.onload = () => {
    getViewCreateStore()
    document.querySelector('.view-store').addEventListener('click', function (event) {
        console.log(event) 
        getViewStore()      
    })    
    document.querySelector("#back-create-store").addEventListener('click', async function (event) {
        getViewCreateStore()
    })
}