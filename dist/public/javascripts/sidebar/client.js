const crudAPI = new APIHandler(`${document.location.origin}/api`);
async function getViewCreateCliente(){
    document.querySelector("#back-create-client").style.display = "none"
    document.querySelector(".view-client-table").style.display = "none"
    document.querySelector(".view-buy-create-client").style.display = "block"
    document.querySelector(".view-client").style.display = "block"
    document.querySelector(".view-update-client").style.display = "none"
    document.querySelector('.create-client').addEventListener('click', async function (event) {
        event.stopImmediatePropagation();
        event.preventDefault()
        let nameClient = document.querySelector("#name-id").value
        let emailClient = document.querySelector("#email-id").value
        let bankClient = document.querySelector("#bank-id").value
        let clabeClient = document.querySelector("#clabe-id").value
        if (nameClient !== "" && emailClient !== "" && clabeClient !== "" ){
            console.log(nameClient,emailClient,bankClient,clabeClient)
            await crudAPI.createClient(nameClient,emailClient,bankClient,clabeClient).then((result)=>{
                document.querySelector("#name-id").value="";
                document.querySelector("#email-id").value="";
                document.querySelector("#clabe-id").value="";
            }).catch((error)=>console.log(error))
        }else{
            alert("Todos los campos deben estar llenos");
        } 
    });
}
async function createTableClient (){
    let table = document.querySelector(".table-client")
    document.querySelector(".title-tables").innerHTML = "Clientes y Proveedores"
    table.caption.innerHTML = `Clientes`
    table.tHead.innerHTML = '<tr> <th scope="col">Nombre</th> <th scope="col">Correo</th> <th scope="col">Banco</th> <th scope="col">Clabe</th> </tr>'
    table.tBodies[0].innerHTML = ""
    await crudAPI.getAllClient().then((resultClient)=>{
        let dataClient =resultClient['client'] 
        for (let i = 0 ;i<dataClient.length;i++){
            let stringTableOrder = ''
            stringTableOrder = `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIdClient('${dataClient[i].id}','${dataClient[i].name}','${dataClient[i].email}','${dataClient[i].bank}','${dataClient[i].clabe}')">${dataClient[i].name}</button></td> <td>${dataClient[i].email}</td> <td>${dataClient[i].bank}</td> <td>${dataClient[i].clabe}</td></tr>`
            table.tBodies[0].innerHTML += stringTableOrder
        }
    }).catch((error)=>console.log(error))
   
}
function clikedIdClient(id,name,email,bank,clabe){
    document.querySelector("#back-create-client").style.display = "none"
    document.querySelector(".view-client-table").style.display = "none"
    document.querySelector(".view-buy-create-client").style.display = "none"
    document.querySelector(".view-client").style.display = "block"
    document.querySelector(".view-update-client").style.display = "block"
    document.querySelector("#name-id-update").value = name
    document.querySelector("#email-id-update").value = email
    document.querySelector("#bank-id-update").value = bank
    document.querySelector("#clabe-id-update").value = clabe
    document.querySelector("#id-update").value = id
    document.querySelector('.update-client').addEventListener('click', async function (event) {
        event.stopImmediatePropagation();
        event.preventDefault()
        let nameClient = document.querySelector("#name-id-update").value
        let emailClient = document.querySelector("#email-id-update").value
        let bankClient = document.querySelector("#bank-id-update").value
        let clabeClient = document.querySelector("#clabe-id-update").value
        let idClient = document.querySelector("#id-update").value
        if (nameClient !== "" && emailClient !== "" && clabeClient !== "" ){
            console.log(nameClient,emailClient,bankClient,clabeClient)
            await crudAPI.updateClient(idClient, nameClient,emailClient,bankClient,clabeClient).then((result)=>{
                getViewClient()     
            }).catch((error)=>console.log(error))
        }else{
            alert("Todos los campos deben estar llenos");
        } 

    })




}
function getViewClient(){
    document.querySelector("#back-create-client").style.display = "block"
    document.querySelector(".view-client-table").style.display = "block"
    document.querySelector(".view-buy-create-client").style.display = "none"
    document.querySelector(".view-client").style.display = "none"
    document.querySelector(".view-update-client").style.display = "none"
    createTableClient()
}

getViewCreateCliente()
window.onload = () => {
    getViewCreateCliente()
    document.querySelector('.view-client').addEventListener('click', function (event) {
        getViewClient()         
    })    
    document.querySelector("#back-create-client").addEventListener('click', async function (event) {
        getViewCreateCliente()
    })
}