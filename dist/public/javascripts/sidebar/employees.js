const crudAPI = new APIHandler(`${document.location.origin}/api`);
async function updateTable(type){
    document.querySelector(".container-table-employees").style.display = "block";
    let table = document.querySelector(".table-employees")
    table.tHead.innerHTML = '<tr> <th scope="col">Nombre</th> <th scope="col">Correo</th> <th scope="col">Almacen</th> <th scope="col">Puesto</th> <th scope="col"></th> <th scope="col">Se acepta?</th> </tr>'
    table.tBodies[0].innerHTML = ""
    document.querySelector("#view-employees-request").style.display="none"
    document.querySelector("#back-employees").style.display = "block"
    document.querySelector("#view-employees-response").style.display="block"
    if (type === "inactive"){
        table.caption.innerHTML = `Empleados Inactivos`
        document.querySelector("#view-employees-response").innerHTML = "Enviar Respuesta"
    }else{
        table.caption.innerHTML = ``
        document.querySelector("#view-employees-response").innerHTML = "Actualizar Empleado"
    }
    let stores= document.createElement("select");
    const allStores = await crudAPI.getAllStore()
    .then((data)=> {
        for (let i = 0; i<data["Store"].length;i++){
            let option = document.createElement("option");
            option.text=data["Store"][i].name
            option.value =  data["Store"][i].id
            stores.add(option)
        }
    })
    let infoStore = document.createElement("td")
    infoStore.appendChild(stores)
    let infoRole = `<select><option value="Regular">Regular</option><option value="Admin">Administrador</option><option value="SuperAdmin">Super Administrador</option> <option value="Basic">Iregular</option></select>`
    return([infoStore,infoRole])
}

async function getEmployees(){
    document.querySelector(".container-table-employees").style.display = "block";
    let table = document.querySelector(".table-employees")
    table.caption.innerHTML = `Empleados`
    table.tHead.innerHTML = '<tr> <th scope="col">Nombre</th> <th scope="col">Almacen</th> <th scope="col">Puesto</th> </tr>'
    table.tBodies[0].innerHTML = ""
    document.querySelector("#view-employees-request").style.display="block"
    document.querySelector("#view-employees-response").style.display="none"
    document.querySelector("#back-employees").style.display = "none"

    const allUser = await crudAPI.getAllUserActive()
        .then((data)=> {
            console.log(data)
            for (i=0;i<data["user"].length;i++){
                if (data["user"][i].store){
                    table.tBodies[0].innerHTML += `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIds('${data["user"][i].id}')">${data["user"][i].name}</button></td> <td>${data["user"][i].store.name}</td> <td>${data["user"][i].role} </td> </tr>`
                } else {
                    table.tBodies[0].innerHTML += `<tr> <td><button type="button" class="btn btn-outline-info btn-sm" onclick="clikedIds('${data["user"][i].id}')">${data["user"][i].name}</button></td> <td>Todos</td> <td>${data["user"][i].role} </td> </tr>`
                
                }
               
            }
        }).catch()
}
async function getEmployeesInactive(){    
    const allUser = await crudAPI.getAllUserInactive()
        .then(async (user)=> {
            await updateTable("inactive").then((infoTable)=>{
                console.log(user)
                let table = document.querySelector(".table-employees")
                for (i=0;i<user["user"].length;i++){
                table.tBodies[0].innerHTML += `<tr> <td class ='${user["user"][i].id}'>${user["user"][i].name}</td><td class='${user["user"][i].email}'>${user["user"][i].email}</td> <td>${infoTable[0].innerHTML}</td> <td>${infoTable[1]}</td> <td><input type="checkbox" id="status-employees" value="StatusEmployees"> </td> </tr>`
            }
            })
        }).catch()
}

async function clikedIds(id) {
    await crudAPI.getOneUser(id).then(async (user)=>{
        await updateTable("active").then((infoTable)=>{
            let table = document.querySelector(".table-employees")
            table.tBodies[0].innerHTML += `<tr> <td class ='${user.id}' >${user.name}</td> <td class='${user.email}'>${user.email}</td>  <td>${infoTable[0].innerHTML}</td> <td>${infoTable[1]}</td> <td>Valido</td> </tr>`
        })
    })
    .catch((error)=>console.log(error))
}   

async function sendResponseEmployes(){
    let table = document.querySelector(".table-employees")
    console.log(table.tBodies[0].childNodes[0].childNodes)
    if (table.caption.innerHTML !== `Empleados Inactivos`){
        let id =table.tBodies[0].childNodes[0].childNodes[1].className
            let name = table.tBodies[0].childNodes[0].childNodes[1].innerHTML
            let email = table.tBodies[0].childNodes[0].childNodes[3].className
            let store =table.tBodies[0].childNodes[0].childNodes[5].childNodes[0].value
            let role = table.tBodies[0].childNodes[0].childNodes[7].childNodes[0].value
            let status = true
            await crudAPI.updateUser(id, role, status,store,email,name)
            .then((data)=>console.log(data))
            .catch((error)=>console.log(error))
    }else{
        for (let i = 0; i<table.tBodies[0].childNodes.length;i++){
            let id =table.tBodies[0].childNodes[i].childNodes[1].className
            let name = table.tBodies[0].childNodes[i].childNodes[1].innerHTML
            let email = table.tBodies[0].childNodes[i].childNodes[2].className
            let store =table.tBodies[0].childNodes[i].childNodes[4].childNodes[0].value
            let role = table.tBodies[0].childNodes[i].childNodes[6].childNodes[0].value
            let status = table.tBodies[0].childNodes[i].childNodes[8].childNodes[0].checked
            await crudAPI.updateUser(id, role, status,store,email,name)
            .then((data)=>console.log(data))
            .catch((error)=>console.log(error))
        }
    }
    
    getEmployees()
}
window.addEventListener('load', () => {
    getEmployees()
    document.getElementById('back-employees').addEventListener('click', function (event) {
      event.preventDefault()
      getEmployees()
    });
    document.getElementById('view-employees-request').addEventListener('click', function (event) {
        event.preventDefault()
        getEmployeesInactive()
      });
      document.getElementById('view-employees-response').addEventListener('click', function (event) {
        event.preventDefault()
        sendResponseEmployes()
      });  
})
