"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var api_store_controllers_1 = require("../controllers/api.store.controllers");
var router = express_1.Router();
router.post('/', api_store_controllers_1.createStore);
router.get('/all', api_store_controllers_1.getAllStore);
router.get('/:id', api_store_controllers_1.getOneStore);
router.put('/:id', api_store_controllers_1.updateStore); //Si no eres administrador solo puedes modificar el status de la orden
router.delete('/id', api_store_controllers_1.deleteStore);
exports.default = router;
