"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var auth_middleware_1 = require("../middlewares/auth.middleware");
var router = express_1.Router();
router.get('/index', function (req, res, next) {
    res.render('sidebar/index', { user: req.user });
});
router.get('/dashboard', auth_middleware_1.permissionAdmin, function (req, res, next) {
    res.render('sidebar/dashboard');
});
router.get('/compras', function (req, res, next) {
    res.render('sidebar/buy', { comesFromAnOrder: false });
});
router.get('/compras/1', function (req, res, next) {
    res.render('sidebar/buy', { comesFromAnOrder: "buy" });
});
router.get('/ventas', function (req, res, next) {
    res.render('sidebar/sell');
});
router.get('/ventas/1', function (req, res, next) {
    res.render('sidebar/sell', { comesFromAnOrder: "sell" });
});
router.get('/inventario', auth_middleware_1.permissionAdmin, function (req, res, next) {
    res.render('sidebar/inventory');
});
router.get('/empleados', auth_middleware_1.permissionSuperAdmin, function (req, res, next) {
    res.render('sidebar/employees');
});
router.get('/clientes', function (req, res, next) {
    res.render('sidebar/providerclient');
});
router.get('/tiendas', auth_middleware_1.permissionSuperAdmin, function (req, res, next) {
    res.render('sidebar/store');
});
exports.default = router;
