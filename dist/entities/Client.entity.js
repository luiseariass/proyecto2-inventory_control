"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Client = exports.Banks = void 0;
var typeorm_1 = require("typeorm");
var Order_entity_1 = require("./Order.entity");
var Banks;
(function (Banks) {
    Banks["AFIRME"] = "Banco Afime";
    Banks["AZTECA"] = "Banco Azteca";
    Banks["BANORTE"] = "Banorte";
    Banks["BBVA"] = "BBVA";
    Banks["HSBC"] = "HSBC";
    Banks["SANTANDER"] = "Banco Santander";
    Banks["INBURSA"] = "Banco Inbursa";
    Banks["SCOTIABANK"] = "Scotiabank";
    Banks["CITIBANAMEX"] = "Citibanamex";
})(Banks = exports.Banks || (exports.Banks = {}));
var Client = /** @class */ (function () {
    function Client() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn("uuid"),
        __metadata("design:type", String)
    ], Client.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Client.prototype, "clabe", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "bank", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Order_entity_1.Order; }, function (order) { return order.client; }),
        typeorm_1.JoinColumn({ name: 'order_id' }),
        __metadata("design:type", Array)
    ], Client.prototype, "order", void 0);
    Client = __decorate([
        typeorm_1.Entity()
    ], Client);
    return Client;
}());
exports.Client = Client;
