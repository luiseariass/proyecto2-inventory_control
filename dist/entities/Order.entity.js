"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Order = exports.TypeOrder = exports.PaymentStatus = exports.Product = void 0;
var typeorm_1 = require("typeorm");
var Client_entity_1 = require("./Client.entity");
var Store_entity_1 = require("./Store.entity");
var User_entity_1 = require("./User.entity");
var Product;
(function (Product) {
    Product["AL_HARD"] = "Hard Aluminum";
    Product["AL_CANS"] = "Aluminum Cans";
    Product["AL_MIXT"] = "Mixt Aluminum";
    Product["CU"] = "Copper";
    Product["BR"] = "Bronze";
    Product["ST"] = "Steel";
})(Product = exports.Product || (exports.Product = {}));
var PaymentStatus;
(function (PaymentStatus) {
    PaymentStatus["PENDING"] = "Pending to pay";
    PaymentStatus["PAID"] = "Paid Out";
})(PaymentStatus = exports.PaymentStatus || (exports.PaymentStatus = {}));
var TypeOrder;
(function (TypeOrder) {
    TypeOrder["PURCHASE"] = "Purcharse Order";
    TypeOrder["SALES"] = "Sales Order";
})(TypeOrder = exports.TypeOrder || (exports.TypeOrder = {}));
var Order = /** @class */ (function () {
    function Order() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn("uuid"),
        __metadata("design:type", String)
    ], Order.prototype, "id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return User_entity_1.User; }, function (user) { return user.email; }),
        typeorm_1.JoinColumn({ name: 'user_email' }),
        __metadata("design:type", User_entity_1.User)
    ], Order.prototype, "orderGenerator", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client_entity_1.Client; }, function (client) { return client.order; }),
        typeorm_1.JoinColumn({ name: 'client_name' }),
        __metadata("design:type", Client_entity_1.Client)
    ], Order.prototype, "client", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Store_entity_1.Store; }, function (store) { return store.user; }),
        typeorm_1.JoinColumn({ name: 'store_name' }),
        __metadata("design:type", Store_entity_1.Store)
    ], Order.prototype, "store", void 0);
    __decorate([
        typeorm_1.Column("simple-array"),
        __metadata("design:type", Array)
    ], Order.prototype, "product", void 0);
    __decorate([
        typeorm_1.Column("simple-array"),
        __metadata("design:type", Array)
    ], Order.prototype, "price", void 0);
    __decorate([
        typeorm_1.Column("simple-array"),
        __metadata("design:type", Array)
    ], Order.prototype, "quantity", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", Date)
    ], Order.prototype, "create_at", void 0);
    __decorate([
        typeorm_1.Column({
            type: "enum",
            enum: PaymentStatus,
            default: PaymentStatus.PENDING
        }),
        __metadata("design:type", String)
    ], Order.prototype, "status", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], Order.prototype, "proofOfPayment", void 0);
    __decorate([
        typeorm_1.Column({
            type: "enum",
            enum: TypeOrder,
        }),
        __metadata("design:type", String)
    ], Order.prototype, "typeOrder", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "numeric" }),
        __metadata("design:type", Number)
    ], Order.prototype, "amount", void 0);
    __decorate([
        typeorm_1.CreateDateColumn({ nullable: true }),
        __metadata("design:type", Date)
    ], Order.prototype, "create_colum_at", void 0);
    Order = __decorate([
        typeorm_1.Entity()
    ], Order);
    return Order;
}());
exports.Order = Order;
