"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.Role = void 0;
var typeorm_1 = require("typeorm");
var Order_entity_1 = require("./Order.entity");
var Store_entity_1 = require("./Store.entity");
var Role;
(function (Role) {
    Role["BASIC"] = "Basic";
    Role["REGULAR"] = "Regular";
    Role["ADMIN"] = "Admin";
    Role["SUPERADMIN"] = "SuperAdmin";
})(Role = exports.Role || (exports.Role = {}));
var User = /** @class */ (function () {
    function User() {
    }
    __decorate([
        typeorm_1.PrimaryGeneratedColumn("uuid"),
        __metadata("design:type", String)
    ], User.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], User.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "googleId", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true }),
        __metadata("design:type", String)
    ], User.prototype, "facebookId", void 0);
    __decorate([
        typeorm_1.Column({
            type: "enum",
            enum: Role,
            default: Role.BASIC
        }),
        __metadata("design:type", String)
    ], User.prototype, "role", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Order_entity_1.Order; }, function (order) { return order.orderGenerator; }),
        typeorm_1.JoinColumn({ name: 'order_id' }),
        __metadata("design:type", Array)
    ], User.prototype, "order", void 0);
    __decorate([
        typeorm_1.Column({ default: false }),
        __metadata("design:type", Boolean)
    ], User.prototype, "status", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Store_entity_1.Store; }, function (store) { return store.user; }),
        typeorm_1.JoinColumn({ name: 'store_id' }),
        __metadata("design:type", Store_entity_1.Store)
    ], User.prototype, "store", void 0);
    User = __decorate([
        typeorm_1.Entity()
    ], User);
    return User;
}());
exports.User = User;
